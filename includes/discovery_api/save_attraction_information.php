<?php

/*==========================================
Save attraction Information
---
Anytime an attraction post is saved then we grab data about that attraction from the Discovery API. 
All attraction data is saved in the WP database for this post as ACF fields.
==========================================*/

function save_attraction_information($post_id) {

	// Save the attraction id so we can query the Discovery API.

    $attraction_id = get_field('attraction_id', $post_id);

    // Save the posts override values. TM team can decide to manually override certain values returned from the API.
    // This is in case the API returns null or incorrect values.

    $api_overrides = get_field('attraction_api_overrides', $post_id);

    if(!$api_overrides) {
        $api_overrides = [];
    }
    
    /*==========================================
    Get this attractions information from the Discovery API.
    ==========================================*/

	$attraction_query_params = array('apikey' => 'FvYYASSbYvnjisVLWo5ZmUhfUgMVHRpF');

    $attraction_query_string = 'https://app.ticketmaster.com/discovery/v2/attractions/' . $attraction_id . '.json?' . http_build_query($attraction_query_params);
    
    $tm_response = @file_get_contents($attraction_query_string);

    // Check if a attraction id was entered or if the response actually rendered results.
    // If it didn't then set the attraction data to an empty array.

    if(empty($attraction_id) || empty($tm_response)) {
        $attraction_data = [];
    }
    else {
        $attraction_data = json_decode($tm_response);
    }

    /*==========================================
    Save all of our returned data from the API into a custom object.
    We will run a loop on that object to check if we should save it to the post.
    ==========================================*/

    $attraction_fields_array = new stdClass;

    // Name

    if(isset($attraction_data->name)) {
        $attraction_fields_array->attraction_name = $attraction_data->name;
    };

    // URL

    if(isset($attraction_data->url)) {
        $attraction_fields_array->attraction_tickets_url = $attraction_data->url;
    };

    // Image

    if(isset($attraction_data->images)) {

        // Save all of the attraction images
        $attraction_images = $attraction_data->images;

        // Filter for images that are above 600px
        $filtered_attraction_images = array_filter($attraction_images, function($image){ 
            return ((int)$image->width) >= 600;
        });

        // Reset the filtered array indexes/keys
        $filtered_attraction_images = array_values($filtered_attraction_images);

        // Save the first image in the array
        $attraction_fields_array->attraction_image = $filtered_attraction_images[0]->url;

    };
    
    // Loop through each field and save its data.

    foreach ($attraction_fields_array as $name => $value) {

        // If the user has chosen not to override this value then update the field value with data we pulled from the API.
        
        if(!in_array($name, $api_overrides)) {
            update_field( $name, $value, $post_id );
        }

    }
    
}

/*==========================================
Save Post Action
---
Anytime a post that pulls data from the Discovery API is saved then run the appropriate function.
==========================================*/

add_action( 'save_post', function($post_id, $post, $update){

    // Check if the post type is minimaster event and if it is being updated (i.e not creating a new post). If not then just return.

	$post_type = get_post_type($post_id);

	if($post_type == 'tm_minimaster_event' && $update) {
		save_attraction_information($post_id);
    }
    else {
        return;
    }

}, 10, 3);

/*==========================================
Really Simple CSV Importer Filter
---
After a post has been imported then run the save attraction information function.
==========================================*/

add_filter( 'really_simple_csv_importer_post_saved', function($post) {

    if($post->post_type === 'tm_minimaster_event') {
        save_attraction_information($post->ID);
    }

}, 10, 1);

?>