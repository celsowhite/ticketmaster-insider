<?php

/*==========================================
Save Event Information
---
Anytime an event post is saved then we grab data about that event from the Discovery API. 
All event data is saved in the WP database for the post as ACF fields.
==========================================*/

function save_event_information($post_id) {

	// Save the event id so we can query the Discovery API.

    $event_id = get_field('event_id', $post_id);

    // Save the posts override values. TM team can decide to manually override certain values returned from the API.
    // This is in case the API returns null or incorrect values.

    $api_overrides = get_field('event_api_overrides', $post_id);

    if(!$api_overrides) {
        $api_overrides = [];
    }
    
    /*==========================================
    Get this events information from the Discovery API.
    ==========================================*/

	$event_query_params = array('apikey' => 'FvYYASSbYvnjisVLWo5ZmUhfUgMVHRpF');

    $event_query_string = 'https://app.ticketmaster.com/discovery/v2/events/' . $event_id . '.json?' . http_build_query($event_query_params);
    
    $tm_response = @file_get_contents($event_query_string);

    // Check if a event id was entered or if the response actually rendered results.
    // If it didn't then set the event data to an empty array.

    if(empty($event_id) || empty($tm_response)) {
        $event_data = [];
    }
    else {
        $event_data = json_decode($tm_response);
    }

    /*==========================================
    Save all of our returned data from the API into a custom object.
    We will run a loop on that object to check if we should save it to the post.
    ==========================================*/

    $event_fields_array = new stdClass;

    // Name

    if(isset($event_data->name)) {
        $event_fields_array->event_name = $event_data->name;
    };

    // URL

    if(isset($event_data->url)) {
        $event_fields_array->event_tickets_url = $event_data->url;
    };

    // Start Date

    if(isset($event_data->dates->start->localDate)) {
        $event_fields_array->event_start_date = $event_data->dates->start->localDate;
    };

    // End Date

    if(isset($event_data->dates->end->localDate)) {
        $event_fields_array->event_end_date = $event_data->dates->end->localDate;
    };

    // Venue Name

    if(isset($event_data->_embedded->venues[0]->name)) {
        $event_fields_array->event_venue_name = $event_data->_embedded->venues[0]->name;
    };

    // Venue City

    if(isset($event_data->_embedded->venues[0]->city->name)) {
        $event_fields_array->event_venue_city = $event_data->_embedded->venues[0]->city->name;
    };

    // Venue State

    if(isset($event_data->_embedded->venues[0]->state->stateCode)) {
        $event_fields_array->event_venue_state = $event_data->_embedded->venues[0]->state->stateCode;
    };

    // Venue Country

    if(isset($event_data->_embedded->venues[0]->country->name)) {
        $event_fields_array->event_venue_country = $event_data->_embedded->venues[0]->country->name;
    };

    // Image

    if(isset($event_data->images)) {

        // Save all of the event images
        $event_images = $event_data->images;

        // Filter for images that are above 600px
        $filtered_event_images = array_filter($event_images, function($image){ 
            return ((int)$image->width) >= 600;
        });

        // Reset the filtered array indexes/keys
        $filtered_event_images = array_values($filtered_event_images);

        // Save the first image in the array
        $event_fields_array->event_image = $filtered_event_images[0]->url;
        
    };

    // Attractions

    $event_attraction_names = array();

    if(isset($event_data->_embedded->attractions)) {

        $event_attractions = $event_data->_embedded->attractions;

        foreach($event_attractions as $attraction) {
            array_push($event_attraction_names, $attraction->name);
        }

        $event_fields_array->event_attractions = implode(", ", $event_attraction_names);

    };
    
    // Loop through each field and save its data.

    foreach ($event_fields_array as $name => $value) {

        // If the user has chosen not to override this value then update the field value with data we pulled from the API.
        
        if(!in_array($name, $api_overrides)) {
            update_field( $name, $value, $post_id );
        }

    }
    
}

/*==========================================
Save Post Action
---
Anytime a post that pulls data from the Discovery API is saved then run the appropriate function.
==========================================*/

add_action( 'save_post', function($post_id, $post, $update){

    // Check if the post type is festival and if it is being updated (i.e not creating a new post). If not then just return.

	$post_type = get_post_type($post_id);

	if($post_type == 'tm_festival' && $update) {
		save_event_information($post_id);
    }
    else {
        return;
    }

}, 10, 3);

/*==========================================
Really Simple CSV Importer Filter
---
After a post has been imported then run the save event information function.
==========================================*/

add_filter( 'really_simple_csv_importer_post_saved', function($post) {

    if($post->post_type === 'tm_festival') {
        save_event_information($post->ID);
    }

}, 10, 1 );

?>