<?php

/*================================= 
TM Get Post Thumbnail
=================================*/

function tm_get_post_thumbnail($post_id, $size) {

	// Logic for getting the post thumbnail. Some legacy posts do not have featured images. They have a 'fallback' module image.
	// This function helps pull the correct image for the post.

	// If the post has a featured image then pull that image.

	if(has_post_thumbnail($post_id)) {
		$post_image = get_the_post_thumbnail_url($post_id, $size);
	}

	// Else pull the legacy module image.

	else {
		$post_image = get_field('module_image', $post_id);
	}

	return $post_image;

};

/*================================= 
Custom Title/Excerpt Lengths
=================================*/

// Title

function tm_title_trim($title) {
	$characters = get_field('title_character_limit', 'option');
	return mb_strimwidth($title, 0, $characters, '...');
}

// Excerpt

function tm_excerpt_trim($excerpt) {
	$characters = get_field('excerpt_character_limit', 'option');
	return mb_strimwidth($excerpt, 0, $characters, '...');
}

/*================================= 
Category Link by Slug
=================================*/

function category_link_by_slug($slug) {

	// Get the ID of a given category

    $category_object = get_category_by_slug($slug );
 
	// Get the URL of this category
	
	$category_link = get_category_link( $category_object );

	return $category_link;
	
}

/*================================= 
Page Link by Slug
=================================*/

function page_link_by_slug($slug) {

	// Get the post object of this page

    $page_object = get_page_by_path($slug);
 
	// Get the URL of this page
	
	$page_link = get_page_link( $page_object );

	return $page_link;
	
}

/*================================= 
Page ID by Slug
=================================*/

function tm_page_id_by_slug($slug) {

	// Get the post object of this page

    $page_object = get_page_by_path($slug);
 
	// Get the ID of this page
	
	$page_id = $page_object->ID;

	return $page_id;
	
}

/*================================= 
TM Event Date Format
---
Insider uses a consistent format for dates when displaying events.
This helper function ensures that dates are always returned in the same format.
Note: Some dates are set dynamically using js.
=================================*/

function tm_event_date_format($date) {

	$dateObject = new DateTime($date);
	$formattedDate = $dateObject->format('D, M j');
	return $formattedDate;

}

/*================================= 
TM Get Page Type
---
In a few instances we need to know the specific category of the page/post we are on.
This is so we can display the appropriate ad or sidebar.
=================================*/

function tm_get_page_type($page_object) {

	// Check that the page object is set. Returns 'null' on the search page.

	if(isset($page_object)) {

		// If a post then check the tags/categories to find the page type

		if($page_object->post_type === 'post') {

			// Save post tag slugs into an array.

			$tag_slugs = array();
			$post_tags = get_the_tags($page_object->ID);
			if($post_tags) {
				foreach($post_tags as $tag) {
					$tag_slugs[] = $tag->slug;
				}
			}

			// Save post category slugs into an array.

			$category_slugs = array();
			$post_categories = get_the_category($page_object->ID);
			if($post_categories) {
				foreach($post_categories as $category) {
					$category_slugs[] = $category->slug;
				}
			}

			if(in_array('new-music', $tag_slugs)) {
				$page_type = 'new-music';
			}
			elseif(in_array('music-festivals', $category_slugs)) {
				$page_type = 'festival';
			}
			elseif(in_array('family', $category_slugs)) {
				$page_type = 'minimaster';
			}
			else {
				$page_type = '';
			}

		}

		// Else then checking a page or archive

		else {

			if(is_tag() && $page_object->slug === 'new-music') {
				$page_type = 'new-music';
			}
			elseif ($page_object->post_name === 'festivals') {
				$page_type = 'festival';
			}
			elseif ($page_object->post_name === 'minimaster' || $page_object->post_name === 'minimaster-events' || $page_object->post_name === 'minimaster-videos') {
				$page_type = 'minimaster';
			}
			else {
				$page_type = '';
			}

		}

	}
	else {
		$page_type = '';
	}

	return $page_type;
	
}

/*================================= 
Image ID to URL
=================================*/

// Function to take an image ID parameter and pull the url for a specific size of the image.

function image_id_to_url($imageID, $imageSize) {
	$image = wp_get_attachment_image_src($imageID, $imageSize);
	$image_url = $image[0];
	return $image_url;
}

/*================================= 
Linked Category Name
=================================*/

// Given a single category object. Return the name of the category linked to its category page.

function tm_linked_category_name($category) {

	$category_name = $category->cat_name;
	$category_id = $category->term_id;
	$category_link = get_category_link($category_id);
							
	echo '<a href="' . $category_link . '">' . $category_name . '</a>';

}

/*================================= 
Category List
=================================*/

// Create a comma separated list of terms from a specific category
// First variable is the post ID and the second is the registered taxonomy name.

function category_terms_list($postID, $category) {
	$post_terms = get_the_terms($postID, $category);
	$post_terms_list_array = array();
	if($post_terms && ! is_wp_error($post_terms)):
		foreach($post_terms as $term) {
			$post_terms_list_array[] = '<a href="' . get_term_link($term->term_id) . '">' . $term->name . '</a>'; 
		}
		return implode(', ', $post_terms_list_array);
	endif;
}

?>