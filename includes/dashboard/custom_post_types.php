<?php

/*==========================================
FESTIVALS
==========================================*/

// Post Type

function custom_post_type_festivals() {

	$labels = array(
		'name'                => ('Festivals'),
		'singular_name'       => ('Festivals'),
		'menu_name'           => ('Festivals'),
		'parent_item_colon'   => (''),
		'all_items'           => ('All Festivals'),
		'view_item'           => ('View Festivals'),
		'add_new_item'        => ('Add New Festivals'),
		'add_new'             => ('Add New'),
		'edit_item'           => ('Edit Festivals'),
		'update_item'         => ('Update Festivals'),
		'search_items'        => ('Search Festivals'),
		'not_found'           => ('Not Found'),
		'not_found_in_trash'  => ('Not found in Trash'),
	);
	
	$args = array(
		'label'               => ('Festivals'),
		'description'         => ('Festivals'),
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor', 'thumbnail', 'excerpt'),
		'hierarchical'        => false,
		'rewrite'             => array('slug' => 'festival'),
		'taxonomies'          => array(),
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 5,
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'post',
		'menu_position'       => 5,
		'menu_icon'  		  => 'dashicons-format-audio',
	);

	register_post_type( 'tm_festival', $args );

}

add_action( 'init', 'custom_post_type_festivals', 0 );

/*==========================================
Minimaster Events
==========================================*/

// Post Type

function custom_post_type_minimaster_events() {

	$labels = array(
		'name'                => ('Minimaster Events'),
		'singular_name'       => ('Minimaster Event'),
		'menu_name'           => ('Minimaster'),
		'parent_item_colon'   => (''),
		'all_items'           => ('Events'),
		'view_item'           => ('View Event'),
		'add_new_item'        => ('Add New Event'),
		'add_new'             => ('Add New'),
		'edit_item'           => ('Edit Event'),
		'update_item'         => ('Update Event'),
		'search_items'        => ('Search Events'),
		'not_found'           => ('Not Found'),
		'not_found_in_trash'  => ('Not found in Trash'),
	);
	
	$args = array(
		'label'               => ('Events'),
		'description'         => ('Events'),
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor', 'thumbnail'),
		'hierarchical'        => false,
		'rewrite'             => array('slug' => 'minimaster-event'),
		'taxonomies'          => array('tm_minimaster_event_type', 'post_tag'),
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 5,
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'post',
		'menu_position'       => 6,
		'menu_icon'  		  => 'dashicons-calendar-alt',
	);

	register_post_type( 'tm_minimaster_event', $args );

}

add_action( 'init', 'custom_post_type_minimaster_events', 0 );

// Event Types

function add_minimaster_event_taxonomy() {

	$labels = array(
		'name' => ('Event Type'),
      	'singular_name' => ('Event Types'),
      	'search_items' =>  ('Search Types' ),
      	'all_items' => ('All Event Types' ),
      	'parent_item' => ('Parent Type' ),
      	'parent_item_colon' => ('Parent Type:' ),
      	'edit_item' => ('Edit Type' ),
      	'update_item' => ('Update Type' ),
      	'add_new_item' => ('Add New Type' ),
      	'new_item_name' => ('New Type' ),
      	'menu_name' => ('Types' ),
	);

	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'show_in_nav_menus' => true,
		'query_var'         => true,
		'rewrite'           => array(),
	);

	register_taxonomy( 'tm_minimaster_event_type', array('tm_minimaster_event'), $args );

}

add_action( 'init', 'add_minimaster_event_taxonomy', 0 );

/*==========================================
AUTHORS
==========================================*/

// Post Type

function custom_post_type_authors() {

	$labels = array(
		'name'                => ('Authors'),
		'singular_name'       => ('Authors'),
		'menu_name'           => ('Authors'),
		'parent_item_colon'   => (''),
		'all_items'           => ('All Authors'),
		'view_item'           => ('View Authors'),
		'add_new_item'        => ('Add New Authors'),
		'add_new'             => ('Add New'),
		'edit_item'           => ('Edit Authors'),
		'update_item'         => ('Update Authors'),
		'search_items'        => ('Search Authors'),
		'not_found'           => ('Not Found'),
		'not_found_in_trash'  => ('Not found in Trash'),
	);
	
	$args = array(
		'label'               => ('Authors'),
		'description'         => ('Authors'),
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor', 'thumbnail', 'excerpt'),
		'hierarchical'        => false,
		'rewrite'             => array('slug' => 'author'),
		'taxonomies'          => array(),
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 5,
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'post',
		'menu_position'       => 20,
		'menu_icon'  		  => 'dashicons-admin-users',
	);

	register_post_type( 'tm_author', $args );

}

add_action( 'init', 'custom_post_type_authors', 0 );

?>