<?php

/*==========================================
REGISTER WIDGET AREA
==========================================*/

function tm_widgets_init() {

	// Sidebars

	register_sidebar( array(
		'name'          => esc_html__( 'Default Sidebar', '_s' ),
		'id'            => 'default_sidebar',
		'description'   => 'Default sidebar displayed across all single posts.',
		'before_widget' => '<aside id="%1$s" class="tm_widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h1 class="widget_title">',
		'after_title'   => '</h1>',
	) );

	register_sidebar( array(
		'name'          => esc_html__( 'Minimaster Sidebar', '_s' ),
		'id'            => 'minimaster_sidebar',
		'description'   => 'Displayed on Minimaster single post pages.',
		'before_widget' => '<aside id="%1$s" class="tm_widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h1 class="widget_title">',
		'after_title'   => '</h1>',
	) );

	register_sidebar( array(
		'name'          => esc_html__( 'Festivals Sidebar', '_s' ),
		'id'            => 'festivals_sidebar',
		'description'   => 'Displayed on Festival single post pages.',
		'before_widget' => '<aside id="%1$s" class="tm_widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h1 class="widget_title">',
		'after_title'   => '</h1>',
	) );

	register_sidebar( array(
		'name'          => esc_html__( 'New Music Sidebar', '_s' ),
		'id'            => 'new_music_sidebar',
		'description'   => 'Displayed on New Music single post pages.',
		'before_widget' => '<aside id="%1$s" class="tm_widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h1 class="widget_title">',
		'after_title'   => '</h1>',
	) );
	
	// Register Widgets

	register_widget( 'upcoming_festivals_widget' );

}

add_action( 'widgets_init', 'tm_widgets_init' );

/*==========================================
WIDGETS
==========================================*/

// Upcoming Festivals Widget

class upcoming_festivals_widget extends WP_Widget {

	/*== Register Widget with Wordpress ==*/

	function __construct() {
		parent::__construct(
			'upcoming_festivals_widget',
			__( 'Upcoming Festivals', 'text_domain' ),
			array( 'description' => __( 'List of 3 upcoming popular festivals.', 'text_domain' ), )
		);
	}

	/*=== Front-end display of widget ===*/

	public function widget( $args, $instance ) {

		$date_now = time();
		$date_yesterday = date('Y-m-d', strtotime('-1 day', $date_now));

		// Popular Festival ID's

		$festivals_page_id = tm_page_id_by_slug('festivals');

		$ids = get_field('popular_festivals', $festivals_page_id, false);

		echo '<h1 class="widget_title">Upcoming Festivals</h1>';

		$upcoming_festivals_args = array(
			'post_type'         => 'tm_festival',
			'post__in'			=> $ids,
			'posts_per_page'    => 3,
			'meta_query'        => array(
				array(
					'key'       => 'event_start_date',
					'value'     => $date_yesterday,
					'compare'   => '>',
					'type'      => 'DATE'
				)
			),
			'meta_key'          => 'event_start_date',
			'orderby'           => 'meta_value',
			'order'             => 'ASC'
		);
		$upcoming_festivals_loop = new WP_Query($upcoming_festivals_args);
		if ( $upcoming_festivals_loop->have_posts() ) : while ( $upcoming_festivals_loop->have_posts() ) : $upcoming_festivals_loop->the_post();

			$inventory_widget_type = 'sidebar';
			include(locate_template('template-parts/card-festival_list_item.php'));

		endwhile; wp_reset_postdata(); endif;

		echo '<a class="link_with_arrow" href="' . page_link_by_slug("festivals") . '">';
        	echo '<h3 class="category_name">View More</h3>';
       		echo '<i class="fal fa-long-arrow-right"></i>';
        echo '</a>';

	}

	/*=== Back-end widget form. ===*/

	public function form( $instance ) {

		echo '<p>Widget custom created. Content will automatically appear when placed in a sidebar.</p>';

	}

	/*=== Sanitize widget form values as they are saved. ===*/

	public function update( $new_instance, $old_instance ) {

		/*=== No fields in this widget ===*/

	}

};

?>