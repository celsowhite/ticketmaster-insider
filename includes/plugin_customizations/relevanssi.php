<?php

/*=============================================
ENHANCE RELEVANSSI POST CONTENT
---
Add custom field information and other data to Relevanssi index
so it can be searched.
=============================================*/

function custom_fields_to_excerpts($content, $post) {

	// Festivals Post Type

	if($post->post_type === 'tm_festival') {

		$festival_venue_name = get_field('event_venue_name', $post->ID);
		$festival_city = get_field('event_venue_city', $post->ID);
		$festival_state = get_field('event_venue_state', $post->ID);
		$festival_attractions = get_field('event_attractions', $post->ID);

		$content .= " " . $festival_attractions . ' ' . $festival_venue_name . ' ' . $festival_city .  ' ' . $festival_state;
			
	}

	return $content;
	
}

add_filter('relevanssi_post_content', 'custom_fields_to_excerpts', 10, 2);

/*=============================================
Ajax Load More & Relevanssi Connect
=============================================*/

function my_alm_query_args_relevanssi($args){

   $args = apply_filters('alm_relevanssi', $args);
   return $args;
   
}

add_filter( 'alm_query_args_relevanssi', 'my_alm_query_args_relevanssi');

?>