<?php

/*==========================================
Add Options Pages
==========================================*/

if( function_exists('acf_add_options_page') ) {
	
	acf_add_options_page(array(
		'page_title' 	=> 'TM Settings',
		'menu_title'	=> 'TM Settings',
		'menu_slug' 	=> 'tm-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> true
	));

	acf_add_options_sub_page(array(
		'page_title' 	=> 'General Settings',
		'menu_title'	=> 'General',
		'parent_slug'	=> 'tm-settings',
	));
	
	acf_add_options_sub_page(array(
		'page_title' 	=> 'Advertisement Settings',
		'menu_title'	=> 'Ads',
		'parent_slug'	=> 'tm-settings',
	));

	acf_add_options_sub_page(array(
		'page_title' 	=> 'Footer Settings',
		'menu_title'	=> 'Footer',
		'parent_slug'	=> 'tm-settings',
	));
		
}

?>