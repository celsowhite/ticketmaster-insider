<?php

/*==========================================
Wordpress Popular Posts
Modifications to the HTML markup of WPP
==========================================*/

function my_custom_popular_posts_html_list( $mostpopular, $instance ){

    // Loop the array of popular posts objects
    
	foreach( $mostpopular as $popular ) {
        
        // Use the sidebar post card layout to render the post.

        $post_id = $popular->id;
		include(locate_template('template-parts/card-sidebar_post.php'));

	}
	
    return;
    
}

add_filter( 'wpp_custom_html', 'my_custom_popular_posts_html_list', 10, 2 );

?>