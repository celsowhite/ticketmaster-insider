<?php

/*==========================================
SAVE THUMBNAIL
---
If importing a csv with the post_thumbnail column then load it into the featured image.
==========================================*/

function really_simple_csv_importer_save_thumbnail_filter( $post_thumbnail, $post, $is_update ) {

    // Import a local file from an FTP directory
    if (!empty($post_thumbnail) && file_exists($post_thumbnail)) {
        $upload_dir   = wp_upload_dir();
        $target_path  = $upload_dir['path'] . DIRECTORY_SEPARATOR . basename($post_thumbnail);
        if (copy($post_thumbnail, $target_path)) {
            $post_thumbnail = $target_path;
        }
    }

    return $post_thumbnail;
}

add_filter( 'really_simple_csv_importer_save_thumbnail', 'really_simple_csv_importer_save_thumbnail_filter', 10, 3 );

/*==========================================
SAVE POST FORMAT
---
If uploading a family post then save the post format as video.
This script was made specifically for importing Minimaster videos.
If uploading other family post formats via CSV then be sure to turn off this functionality.
==========================================*/

add_filter( 'really_simple_csv_importer_post_saved', function($post) {

    if(get_the_category($post->ID)) {

        // Get the category

        $first_category_slug = get_the_category($post->ID)[0]->slug;

        // If a post and the category is family then set post format to video.
        
        if($post->post_type === 'post' && $first_category_slug === 'family') {
            set_post_format($post->ID, 'video');
        }

    }

}, 10, 1 );

?>