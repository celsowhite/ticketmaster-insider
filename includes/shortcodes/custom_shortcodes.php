<?php

/*==========================================
BUTTON
==========================================*/

function tm_button_shortcode( $atts, $content = null ) {

	$a = shortcode_atts( array(
        'url'    => '',
        'color'   => 'blue',
        'new_tab' => 'false'
    ), $atts );

    if($a['new_tab'] === 'true') {
        $new_tab = 'target="_blank"';
    }
    else {
        $new_tab = '';
    }

    return '<a href="' . $a['url'] . '"' . $new_tab . ' class="tm_button ' . $a['color'] . '">' . $content . '</a>';

}

add_shortcode( 'tm_button', 'tm_button_shortcode' );

/*==========================================
TWITTER BLOCKQUOTE
==========================================*/

function tm_twitter_blockquote_shortcode( $atts, $content = null ) {

	return '<blockquote>' . '<p>'. $content . '</p>' . '<a href="" class="twitter_share tm_twitter_blockquote_share" data-text="' . $content . '" data-via="Ticketmaster">Tweet This' . '<i class="fab fa-twitter"></i></a>';

}

add_shortcode( 'tm_twitter_blockquote', 'tm_twitter_blockquote_shortcode' );

/*==========================================
FULL WIDTH IMAGE
==========================================*/

function tm_full_width_image_shortcode( $atts, $content = null ) {

	$a = shortcode_atts( array(
        'src' => '',
    ), $atts );

    return '<div class="full_width_image" style="background-image:url(' . $a["src"] . ') "><p>' . $content . '</p></div>';

}

add_shortcode( 'tm_full_width_image', 'tm_full_width_image_shortcode' );

/*==========================================
New Custom FAQ
---
Updated FAQ shortcode syntax that allows for more flexibility.
==========================================*/

function tm_faq_shortcode( $atts, $content = null ) {

    $a = shortcode_atts( array(
        'title'    => ''
    ), $atts );

    return '<div class="tm_faq"><h5 class="tm_faq_title">' . $a["title"] . '</h5><div class="tm_faq_content">' . $content . '</div></div>';

}

add_shortcode('tm_faq', 'tm_faq_shortcode');

/*==========================================
Legacy FAQ
---
Easy FAQ w/ Expanding Text Plugin used to be installed pre redesign. 
So quite a few posts have their shortcode syntax. Add the syntax and use
the new faq output.
==========================================*/

function bq_faq_start_shortcode( $atts, $content = null ) {

    return '<div class="bq_faq">';

}

function bq_faq_end_shortcode( $atts, $content = null ) {

    return '</div>';

}

add_shortcode( 'bg_faq_start' , 'bq_faq_start_shortcode' );
add_shortcode( 'bg_faq_end' , 'bq_faq_end_shortcode' );

?>