<?php 

/*==========================================
Default Gallery 
Update the default gallery markup to work with Magnific Popup
==========================================*/

function default_gallery_transformation($atts) {
    
    // Get the ID's of the images in the gallery

    $image_ids = explode(',', $atts['ids']);

    // Columns
    // If columns are set then make sure the grid container has the appropriate class name.
    // Defaults to three.

    $columns_name = 'three_columns';

    if(isset($atts['columns'])) {

        $columns = $atts['columns'];

        if ($columns === '1') {
            $columns_name = 'one_column';
        } 
        elseif ($columns === '2') {
            $columns_name = 'two_columns';
        } 
        elseif ($columns === '3') {
            $columns_name = 'three_columns';
        } 
        elseif ($columns === '4') {
            $columns_name = 'four_columns';
        } 
        elseif ($columns == '5') {
            $columns_name = 'five_columns';
        }
        elseif ($columns == '6') {
            $columns_name = 'six_columns';
        }
        elseif ($columns == '7') {
            $columns_name = 'seven_columns';
        }
        elseif ($columns == '8') {
            $columns_name = 'eight_columns';
        }
        elseif ($columns == '9') {
            $columns_name = 'nine_columns';
        }

    }

    // Start the html markup for the slider

    $gallery_output = '<div class="wp_custom_gallery ' . $columns_name . '">';

    // Loop through each image ID to get it's attachment URL to add to our html markup

    foreach($image_ids as $id) {
        $full_image_src = wp_get_attachment_image_src($id, 'large')[0];
        $thumbnail_image_src = wp_get_attachment_image_src($id, 'thumbnail')[0];
        if(isset(get_post($id)->post_excerpt)) {
            $image_caption = htmlspecialchars(get_post($id)->post_excerpt);
        }
        $gallery_output .= '<a href="' . $full_image_src . '" title="' . $image_caption . '"><div class="wp_custom_gallery_thumbnail"><img src="' . $thumbnail_image_src . '" /></div></a>';
    }

    // Finalize the output by closing out our markup and returning it to the post_gallery filter hook

    return $gallery_output . '</div>';

} 

/*==========================================
Flexslider Gallery
Output a flexslider slideshow using WP default gallery shortcode.
==========================================*/

// Function to output the correct gallery markup

function flexslider_gallery_transformation($atts) {
    
    // Get the ID's of the images in the gallery

    $image_ids = explode(',', $atts['ids']);

    // Start the html markup for the slider

    $slider_output = '<div class="tm_gallery_slider_container"><ul class="gallery_slider_thumbnails">';

    // Create the thumbnail nav

    foreach($image_ids as $id) {
        $image_src = wp_get_attachment_image_src($id, 'thumbnail', false);
        $slider_output .= '<li><img src="' . $image_src[0] . '"></li>';
    }

    $slider_output .= '</ul><div class="tm_gallery_slider flexslider"><ul class="slides">';

    // Create the slides

    foreach($image_ids as $id) {
        $image_src = wp_get_attachment_image_src($id, 'large', false);
        $image_caption = get_post($id)->post_excerpt;

        // If there is a caption for this image then show it with the slide

        if($image_caption) {
            $slider_output .= '<li><img src="' . $image_src[0] . '"><p class="slider_caption">' . $image_caption . '</p></li>';
        }
        else {
            $slider_output .= '<li><img src="' . $image_src[0] . '"></li>';
        }
    }

    // Finalize the output by closing out our slider markup and returning it to the post_gallery filter hook

    return $slider_output . '</ul></div></div>';
}

/*==========================================
Post Gallery Filter Hook
==========================================*/

function tm_gallery_shortcode( $output = '', $atts, $instance ) {
    
    $return = $output;

    // If flexslider attribute is set to true in gallery shortcode then retreive custom slider output

    if(isset($atts['flexslider'])) {
        $gallery_markup = flexslider_gallery_transformation($atts);
    }

    // Else if the shortcode has ids attribute then transform the default gallery output
    // Have to check this because potential legacy case of [gallery link="file"] shortcode

    else if(isset($atts['ids'])) {
        $gallery_markup = default_gallery_transformation($atts);
    }

    if(!empty( $gallery_markup)) {
        $return = $gallery_markup;
    }

    return $return;
}

add_filter( 'post_gallery', 'tm_gallery_shortcode', 10, 3 );

?>