<?php

/*=============================================
Render Popular Festivals
---
Festivals list defaults to popular festivals. 
Anytime the user clears results then we default to popular.
=============================================*/

add_action('wp_ajax_render_popular_festivals', 'render_popular_festivals');
add_action('wp_ajax_nopriv_render_popular_festivals', 'render_popular_festivals');

function render_popular_festivals() {

	$date_now = time();
	$date_yesterday = date('Y-m-d', strtotime('-1 day', $date_now));

	// Popular Festival ID's

	$ids = get_field('popular_festivals', 31227, false);

	// Get popular upcoming festivals. Order by ascending start date.

	$args = array(
		'post_type'         => 'tm_festival',
		'post__in'			=> $ids,
	    'meta_query'        => array(
			array(
				'key'       => 'event_start_date',
				'value'     => $date_yesterday,
				'compare'   => '>',
				'type'      => 'DATE'
			)
		),
		'meta_key'          => 'event_start_date',
		'orderby'           => 'meta_value',
		'order'             => 'ASC'
	);

	$query = new WP_Query($args);

	// Loop through the found festivals and create the full markup.

	if($query->have_posts()) {
		while ($query->have_posts()) {
			$query->the_post();
			get_template_part('template-parts/card', 'festival_list_item');
		}
	}

	die();

}

/*=============================================
Festival Filter
---
Users can filter for festivals using a location and month dropdown.
=============================================*/

add_action('wp_ajax_festival_filter', 'festival_filter');
add_action('wp_ajax_nopriv_festival_filter', 'festival_filter');

function festival_filter() {
	
	// Include time period in our results regardless of if a month was selected.
	// If no month is selected then the ajax call will send the date range of the entire year by default.
	
	$start = $_REQUEST['startDate'];
	$end = $_REQUEST['endDate'];

	// If location is set then do a compound query for festivals in that location during a given time period.

	if(isset($_REQUEST['location']) && !empty($_REQUEST['location'])) {

		// Save the location type key

		if($_REQUEST['locationType'] === 'state') {
			$location_key = 'event_venue_state';
		}
		elseif($_REQUEST['locationType'] === 'country') {
			$location_key = 'event_venue_country';
		}

		// Create the query args

		$args = array(
			'post_type'      => 'tm_festival',
			'meta_query'     => array(
				'relation'	    => 'AND',
				array(
					'key'       => 'event_start_date',
					'value'     => array($start, $end),
					'compare'   => 'BETWEEN',
					'type'      => 'DATE'
				),
				array(
					'key'	  	=> $location_key,
					'value'	  	=> $_REQUEST['location'],
					'compare' 	=> '=',
				)
			),
			'meta_key'          => 'event_start_date',
			'orderby'           => 'meta_value',
			'order'             => 'ASC'
		);
		
	}

	// Else location wasn't set then just search by date.

	else {

		$args = array(
			'post_type'      => 'tm_festival',
			'meta_query'     => array(
				array(
					'key'       => 'event_start_date',
					'value'     => array($start, $end),
					'compare'   => 'BETWEEN',
					'type'      => 'DATE'
				),
			),
			'meta_key'          => 'event_start_date',
			'orderby'           => 'meta_value',
			'order'             => 'ASC'
		);

	}
	  
	$query = new WP_Query($args);

	// Loop through the found festivals and create the full markup.

	if($query->have_posts()) {
		while ($query->have_posts()) {
			$query->the_post();
			get_template_part('template-parts/card', 'festival_list_item');
		}
	}

	die();

}

/*=============================================
Festival Search
---
Use Relevanssi to search and return relevant upcoming festivals.
=============================================*/

add_action('wp_ajax_festival_search', 'festival_search');
add_action('wp_ajax_nopriv_festival_search', 'festival_search');

function festival_search() {

	// Cache Today/Yesterday Date

	$date_now = time();
  	$date_yesterday = date('Y-m-d', strtotime('-1 day', $date_now));
	
	$args = array(
		's'                 => $_REQUEST['keyword'],
		'post_type'         => 'tm_festival',
		// Only query upcoming festivals whose start date has not yet passed.
		'meta_query'     => array(
			array(
				'key'       => 'event_start_date',
				'value'     => $date_yesterday,
				'compare'   => '>',
				'type'      => 'DATE'
			)
		)
	);
	  
	$query = new WP_Query($args);

	// Order festival results by date first.

	add_filter( 'relevanssi_hits_filter', 'order_festivals' );

	relevanssi_do_query($query);

	// Loop through the found festivals and create the full markup.

	if($query->have_posts()) {
		while ($query->have_posts()) {
			$query->the_post();
			get_template_part('template-parts/card', 'festival_list_item');
		}
	}

	die();

}

// Helper function to order festival search results by ascending start date.

function order_festivals($hits) {
	
	global $wp_query;

	function cmp($a, $b) {
		return strcmp(get_field('event_start_date', $a->ID), get_field('event_start_date', $b->ID));
	}

	usort($hits[0], 'cmp');

	// Return the reordered hits.

	return $hits;
	
}

?>