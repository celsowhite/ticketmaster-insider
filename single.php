<?php
/**
 * The template for displaying all single posts.
 */

get_header(); ?>

	<main class="main_wrapper">

		<?php while ( have_posts() ) : the_post(); ?>

			<?php get_template_part('template-parts/hero', 'individual_post'); ?>

			<div class="page_content">

				<div class="tm_section background_white">
				
					<div class="small_container">

						<div class="tm_row single_post_body">

							<div class="column_2_3">
								
								<div class="single_post_content">
									
									<div class="wysiwyg">
										<?php the_content(); ?>
										<?php get_template_part('template-parts/post', 'legacy_new_music_content'); ?>
									</div>

									<?php 
									$inventory_widget_id = $post->ID;
									include(locate_template('template-parts/inventory_widget.php')); ?>

									<?php get_template_part('template-parts/post', 'tag_list'); ?>
								
								</div>

							</div>

							<div class="column_1_3">
								
								<div class="single_post_sidebar">
									<?php get_template_part('template-parts/post', 'sidebars'); ?>
								</div>

							</div>

						</div>

						<?php get_template_part('template-parts/card', 'next_post'); ?>
					
					</div>
				
				</div>

				<!-- Related Posts -->
				
				<?php get_template_part('template-parts/post', 'related_posts'); ?>
				
			</div>

		<?php endwhile; ?>

	</main>
	
<?php get_footer(); ?>