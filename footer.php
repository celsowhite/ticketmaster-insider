<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package _s
 */

?>

</div>

<footer class="footer">

	<div class="container">

		<div class="tm_row">
			<?php 
				// Output footer menus
				// Save the available menu locations. To access in our loop and get a specific menu id.
				$locations = get_nav_menu_locations();
				// Loop through each of our footer menus
				for($i=1; $i<=2; $i++) {
					// Save the specific theme location
					$menu_theme_location = 'footer_menu_' . $i;
					$menu_id = $locations[$menu_theme_location];
					// Get the menu object and save the name
					$menu_object = wp_get_nav_menu_object($menu_id);
					$menu_name = $menu_object->name;
					// Output the menu
					echo '<div class="column_1_4">';
					echo '<h3>' . $menu_name . '</h3>';
					wp_nav_menu( array( 'theme_location' => $menu_theme_location, 'container' => '' ) );
					echo '</div>';			
				}
			?>
			<div class="column_1_4 footer_partner_logos">
				<h3>Our Partners</h3>
				<?php the_field('footer_partners', 'option'); ?>
			</div>
			<div class="column_1_4 footer_app_logos">
				<h3>Download Our App</h3>
				<?php the_field('footer_apps', 'option'); ?>
			</div>
		</div>

		<?php the_field('footer_text', 'option'); ?>

	</div>

</footer>

<?php wp_footer(); ?>

<script>

	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

	ga('create', 'UA-XXXXXXXX-X', 'auto');
	ga('send', 'pageview');

</script>

</body>
</html>
