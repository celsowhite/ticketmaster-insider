<?php
/*
Default page template
*/

get_header(); ?>

	<main class="main_wrapper">

		<?php while ( have_posts() ) : the_post(); ?>

			<div class="page_content wysiwyg">
				<div class="tm_section">
					<div class="container">
						<div class="tm_row single_post_body">
							<div class="column_2_3">
								<?php the_content(); ?>
							</div>
							<div class="column_1_3">
								<div class="single_post_sidebar">
									<?php get_template_part('template-parts/post', 'sidebars'); ?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

		<?php endwhile; ?>

	</main>

<?php get_footer(); ?>

