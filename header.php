<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package _s
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

<!-- Favicon -->

<link rel="shortcut icon" type="image/png" href="<?php echo get_template_directory_uri() . '/favicon.png'; ?>" />

<meta name="google-site-verification" content="AK8L9xpueHkFW2EbFyfIpd7d0IGZGU1Da_tjQTLzB4E" />

<!-- Tracking Scripts -->

<?php get_template_part('template-parts/header', 'tracking_scripts'); ?>

<?php wp_head(); ?>

</head>

<body <?php body_class(); ?>>

	<header class="main_header">

		<div class="header_top">

			<div class="container">

				<div class="header_left">
					<a class="header_logo" href="<?php echo esc_url( home_url( '/' ) ); ?>">
						<img src="<?php echo get_template_directory_uri() . '/img/logo/tm_insider_logo.png'; ?>" />
					</a>
					<nav class="main_navigation">
						<?php wp_nav_menu( array( 'theme_location' => 'main_menu', 'menu_id' => 'primary-menu', 'container' => '' ) ); ?>
					</nav>
				</div>

				<div class="header_right">
					<?php if( have_rows('social_profiles', 'option') ): ?>
						<ul class="social_icons">
							<?php while( have_rows('social_profiles', 'option') ): the_row(); ?>
								<li>
									<a href="<?php the_sub_field('link', 'option'); ?>" target="_blank"><i class="<?php the_sub_field('network', 'option'); ?>"></i></a>
								</li>
							<?php endwhile; ?>
						</ul>
					<?php endif; ?>
					<div class="search_icon">
						<span class="circle"></span>
						<span class="handle"></span>
					</div>
				</div>

			</div>

		</div>

		<div class="sub_nav_bar"></div>

	</header>

	<header class="mobile_header">
		<div class="container">
			<a class="logo" href="<?php echo esc_url( home_url( '/' ) ); ?>">
				<img src="<?php echo get_template_directory_uri() . '/img/logo/tm_insider_logo.png'; ?>" />
			</a>
			<span class="menu_icon"></span>
		</div>
	</header>

	<nav class="mobile_navigation">
		<div class="container">
			<?php get_search_form(); ?>
			<?php wp_nav_menu( array( 'theme_location' => 'main_menu', 'menu_id' => 'primary-menu', 'container' => '' ) ); ?>
			<?php if( have_rows('social_profiles', 'option') ): ?>
				<ul class="social_icons">
					<?php while( have_rows('social_profiles', 'option') ): the_row(); ?>
						<li>
							<a href="<?php the_sub_field('link', 'option'); ?>" target="_blank"><i class="<?php the_sub_field('network', 'option'); ?>"></i></a>
						</li>
					<?php endwhile; ?>
				</ul>
			<?php endif; ?>
		</div>
	</nav>

	<?php get_template_part('template-parts/overlay', 'search'); ?>

	<?php get_template_part('template-parts/modal', 'video'); ?>

	<?php get_template_part('template-parts/modal', 'lineup'); ?>

	<div id="content" class="main_content">