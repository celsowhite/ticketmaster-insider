<?php
/**
 * Tag template
 */

get_header(); ?>

	<main id="main post-<?php the_ID(); ?>" class="main_wrapper" role="main">

        <?php 
        $term = get_queried_object();
        $tag_slug = get_query_var('tag');
		if ( have_posts() ) : ?>

            <?php 
            // If featured posts are selected for this term then show the slider in the header.
            // Slides won't effect the offset of posts in the grid.
            if(get_field('featured_posts', $term)): 
            $post_grid_offset = 0;
            $load_more_grid_offset = 11;
            ?>

                <?php get_template_part('template-parts/slider', 'featured_posts'); ?> 

            <?php 
            // Else default to show the most recent post.
            // Post grid offset is effected so we don't repeat show the header post.
            else: 
            $post_grid_offset = 1;
            $load_more_grid_offset = 12;
            ?>

                <?php $post_count = 1; while ( have_posts() ) : the_post(); ?>

                    <?php if($post_count === 1): ?>
                        
                        <?php get_template_part('template-parts/hero', 'featured_post'); ?>

                    <?php break; endif; ?>

                <?php $post_count++; endwhile; ?>

            <?php endif; ?>
            
            <div class="page_content">

                <div class="tm_section">

                    <div class="container">

                        <!-- Sub Navigation -->

                        <?php if( have_rows('sub_navigation', $term) ): ?>

                            <div class="trending_bar">
                                <ul class="tm_pills centered">
                                    <?php while(have_rows('sub_navigation', $term)): the_row(); ?>
                                        <?php $link = get_sub_field('link', $term); ?>
                                        <li>
                                            <a href="<?php echo $link['url'] ?>" target="<?php echo $link['target']; ?>"><?php echo $link['title']; ?></a>
                                        </li>
                                    <?php endwhile; ?>
                                </ul>
                            </div>

                        <?php endif; ?>

                        <h1 class="inpage_header">Latest <?php single_tag_title(); ?></h1>

                        <!-- Special Template Layout -->
                        
                        <?php if(get_field('special_tag_category_template', $term)): ?>

                            <!-- Static Post Grid -->
					
                            <?php
                            $post_count = 1;
                            $grid_loop_args = array ('post_type' => 'post', 'tag' => $tag_slug, 'posts_per_page' => 11, 'offset' => $post_grid_offset);
                            $grid_loop = new WP_Query($grid_loop_args);
                            if ($grid_loop -> have_posts()) : while ($grid_loop -> have_posts()) : $grid_loop -> the_post();
                            ?>

                                <?php if($post_count === 1): ?>
                                    <div class="post_grid two_column">
                                        <?php get_template_part('template-parts/card', 'standard_post'); ?>
                                <?php elseif($post_count === 3): ?>
                                    </div>
                                    <div class="tm_row post_grid_inner_row">
                                        <div class="column_2_3">
                                            <div class="post_grid two_column_inner">
                                                <div class="post_card">
                                                    <div class="post_card_ad_unit">
                                                        <?php get_template_part('template-parts/component', 'rectangle_ad_unit'); ?>
                                                    </div>
                                                </div>
                                                <?php get_template_part('template-parts/card', 'standard_post'); ?>
                                <?php elseif($post_count === 5): ?>
                                                <?php get_template_part('template-parts/card', 'standard_post'); ?>
                                            </div>
                                        </div>
                                        <div class="column_1_3 post_grid_inventory_widget_column">
                                            <?php 
                                            $inventory_widget_id = $term;
                                            include(locate_template('template-parts/inventory_widget.php')); ?>
                                        </div>
                                </div>
                                <?php elseif($post_count === 6): ?>
                                    <div class="post_grid two_column">
                                        <?php get_template_part('template-parts/card', 'standard_post'); ?>
                                <?php elseif($post_count === 7): ?>
                                        <?php get_template_part('template-parts/card', 'standard_post'); ?>
                                    </div>
                                    <div class="tm_row post_grid_inner_row">
                                        <div class="column_1_3">
                                            <div class="post_grid_embed_container">
                                                <?php if(get_field('featured_social_embed', $term)): ?>
                                                    <?php the_field('featured_social_embed', $term); ?>
                                                <?php else: ?>
                                                    <?php the_field('tm_twitter_feed', 'option'); ?>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                <?php elseif($post_count === 8): ?>
                                        <div class="column_2_3">
                                            <div class="post_grid two_column_inner">
                                                <?php get_template_part('template-parts/card', 'standard_post'); ?>
                                <?php elseif($post_count === 11): ?>
                                                <?php get_template_part('template-parts/card', 'standard_post'); ?>
                                            </div>
                                        </div>
                                    </div>
                                <?php else: ?>
                                    <?php get_template_part('template-parts/card', 'standard_post'); ?>
                                <?php endif; ?>

                            <?php $post_count++; endwhile; wp_reset_postdata(); endif; ?>

                            <!-- AJAX Loaded Posts -->
                            
                            <?php

                            // Ajax load more the rest of the posts in this tag.

                            echo do_shortcode('[ajax_load_more 
                                                button_label="<h3 class=\'spaced\'>View More</h3><i class=\'fal fa-long-arrow-down\'></i>"
                                                button_loading_label="<h3 class=\'spaced\'>Loading</h3><i class=\'fal fa-spinner\'></i>"
                                                tag="' . $tag_slug . '"  
                                                transition_container_classes="post_grid alternating" 
                                                theme_repeater="card-standard_post.php" 
                                                posts_per_page="8"
                                                scroll="false" 
                                                offset="' . $load_more_grid_offset . '"]'
                                            ); 

                            ?>

                        <!-- Default Layout -->

                        <?php else: ?>

                            <!-- Pre loaded Post Grid w/ Ad -->
					
                            <?php
                            $post_count = 1;
                            $grid_loop_args = array ('post_type' => 'post', 'tag' => $tag_slug, 'posts_per_page' => 4, 'offset' => $post_grid_offset);
                            $grid_loop = new WP_Query($grid_loop_args);
                            if ($grid_loop -> have_posts()) : while ($grid_loop -> have_posts()) : $grid_loop -> the_post();
                            ?>

                                <?php if($post_count === 1): ?>
                                    <div class="post_grid alternating">
                                        <?php get_template_part('template-parts/card', 'standard_post'); ?>
                                <?php elseif($post_count === 3): ?>
                                        <div class="post_card">
                                            <div class="post_card_ad_unit">
                                                <?php get_template_part('template-parts/component', 'rectangle_ad_unit'); ?>
                                            </div>
                                        </div>
                                        <?php get_template_part('template-parts/card', 'standard_post'); ?>
                                <?php elseif($post_count === 4): ?>
                                        <?php get_template_part('template-parts/card', 'standard_post'); ?>
                                    </div>
                                <?php else: ?>
                                    <?php get_template_part('template-parts/card', 'standard_post'); ?>
                                <?php endif; ?>

                            <?php $post_count++; endwhile; wp_reset_postdata(); endif; ?>
                            
                            <!-- Ajax load more the rest of the posts in this category. -->

                            <?php echo do_shortcode('[ajax_load_more 
                                                button_label="<h3 class=\'spaced\'>View More</h3><i class=\'fal fa-long-arrow-down\'></i>"
                                                button_loading_label="<h3 class=\'spaced\'>Loading</h3><i class=\'fal fa-spinner\'></i>"
                                                tag="' . $tag_slug . '" 
                                                transition_container_classes="post_grid alternating" 
                                                theme_repeater="card-standard_post.php" 
                                                posts_per_page="8"
                                                scroll="false" 
                                                offset="' . $load_more_grid_offset . '"]'
                                            ); 
                            ?>

                        <?php endif; ?>

                    </div>

                </div>

            </div>

		<?php endif; ?>

	</main>

<?php get_footer(); ?>