<?php
/*
Template Name: Minimaster
*/

get_header(); ?>

	<main class="main_wrapper">

		<?php while ( have_posts() ) : the_post(); ?>

            <?php 
            $slider_background_color = 'colorful';
            $sponsor = 'minimaster';
            include(locate_template('template-parts/slider-featured_posts.php'));
            ?>

			<div class="page_content">

                <div class="tm_section background_white">

                    <div class="container">

                        <h1 class="inpage_header">Minimaster Events</h1>

                        <?php get_template_part('template-parts/component', 'minimaster_events_carousel'); ?>

                    </div>
                
                </div>
                
                <div class="tm_section">

                    <?php
                    /* <div class="tm_rectangle_ad_container">
                        <?php get_template_part('template-parts/component', 'rectangle_ad_unit'); ?>
                    </div> */
                    ?>
                
                    <div class="container">
                        
                        <div class="tm_leaderboard_ad_container">
                            <?php get_template_part('template-parts/component', 'leaderboard_ad_unit'); ?>
                        </div>

                        <!-- AJAX Loaded Posts -->
                        
                        <?php
                        // Ajax load more the rest of the posts in this category.
                        $cat = get_query_var('cat');
                        $category = get_category ($cat);
                        $button_label = '<h3>View More</h3><i class="fal fa-long-arrow-right"></i>';
                        echo do_shortcode('[ajax_load_more 
                                            button_label="<h3 class=\'spaced\'>View More</h3><i class=\'fal fa-long-arrow-down\'></i>"
                                            button_loading_label="<h3 class=\'spaced\'>Loading</h3><i class=\'fal fa-spinner\'></i>"
                                            category="family" 
                                            transition_container_classes="post_grid two_column" 
                                            theme_repeater="card-minimaster_post.php" 
                                            posts_per_page="8"
                                            scroll="false"]'
                                        ); 
                        ?>

                    </div>

                </div>

			</div>
			
		<?php endwhile; ?>

	</main>

<?php get_footer(); ?>