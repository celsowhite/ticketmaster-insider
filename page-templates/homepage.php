<?php
/*
Template Name: Homepage
*/

get_header(); ?>

	<main class="main_wrapper">

		<?php while ( have_posts() ) : the_post(); $post_id = get_the_ID(); ?>

			<!-- Slider -->

			<?php get_template_part('template-parts/slider', 'featured_posts'); ?>

			<div class="page_content">
				
				<div class="tm_section">

					<div class="container">

						<!-- Trending Pills -->

						<div class="trending_bar pink centered">
							<h3 class="spaced">Trending</h3>
							<?php 
							$trending_tags = get_field('trending_tags', 'option');
							if( $trending_tags ): ?>
								<ul class="tm_pills pink">
									<?php foreach( $trending_tags as $tag ): ?>
										<li><a href="<?php echo get_term_link( $tag ); ?>">#<?php echo $tag->name; ?></a></li>
									<?php endforeach; ?>
								</ul>
							<?php endif; ?>
						</div>

						<h1 class="inpage_header">Latest</h1>

						<!-- Static Post Grid w/ Inventory Widget -->
						
						<?php
						$post_count = 1;
						$grid_loop_args = array (
							'post_type' => 'post', 
							'posts_per_page' => 5,
							'ignore_sticky_posts' => true
						);
						$grid_loop = new WP_Query($grid_loop_args);
						if ($grid_loop -> have_posts()) : while ($grid_loop -> have_posts()) : $grid_loop -> the_post();
						?>

							<?php if($post_count === 1): ?>
								<div class="post_grid two_column">
									<?php get_template_part('template-parts/card', 'standard_post'); ?>
							<?php elseif($post_count === 3): ?>
								</div>
								<div class="tm_row post_grid_inner_row">
									<div class="column_2_3">
										<div class="post_grid two_column_inner">
											<div class="post_card with_ad">
												<div class="post_card_ad_unit">
													<?php get_template_part('template-parts/component', 'rectangle_ad_unit'); ?>
												</div>
											</div>
											<?php get_template_part('template-parts/card', 'standard_post'); ?>
							<?php elseif($post_count === 5): ?>
											<?php get_template_part('template-parts/card', 'standard_post'); ?>
										</div>
									</div>
									<div class="column_1_3 post_grid_inventory_widget_column">
										<?php 
										$inventory_widget_id = $post_id;
										include(locate_template('template-parts/inventory_widget.php')); 
										?>
									</div>
								</div>
							<?php else: ?>
								<?php get_template_part('template-parts/card', 'standard_post'); ?>
							<?php endif; ?>

						<?php $post_count++; endwhile; wp_reset_postdata(); endif; ?>
					
					</div>
					
					<!-- Upcoming Festivals -->

					<div class="tm_section background_white">

						<div class="container">

							<?php get_template_part('template-parts/component', 'upcoming_festivals_carousel'); ?>

						</div>
					
					</div>

					<div class="tm_section">

						<div class="container">
							
							<!-- Trending Posts -->

							<?php get_template_part('template-parts/component', 'trending_posts'); ?>
							
							<!-- Static Posts Grid w/ Social Embed -->

							<?php
							$post_count = 1;
							$second_grid_loop_args = array (
								'post_type' => 'post', 
								'posts_per_page' => 6, 
								'offset' => 5, 
								'ignore_sticky_posts' => true
							);
							$second_grid_loop = new WP_Query($second_grid_loop_args);
							if ($second_grid_loop -> have_posts()) : while ($second_grid_loop -> have_posts()) : $second_grid_loop -> the_post();
							?>

								<?php if($post_count === 1): ?>
									<div class="post_grid two_column">
										<?php get_template_part('template-parts/card', 'standard_post'); ?>
								<?php elseif($post_count === 2): ?>
										<?php get_template_part('template-parts/card', 'standard_post'); ?>
									</div>
									<div class="tm_row post_grid_inner_row">
										<div class="column_1_3">
											<div class="post_grid_embed_container">
												<?php the_field('featured_social_embed', $post_id); ?>
											</div>
										</div>
								<?php elseif($post_count === 3): ?>
										<div class="column_2_3">
											<div class="post_grid two_column_inner">
												<?php get_template_part('template-parts/card', 'standard_post'); ?>
								<?php elseif($post_count === 6): ?>
												<?php get_template_part('template-parts/card', 'standard_post'); ?>
											</div>
										</div>
									</div>
								<?php else: ?>
									<?php get_template_part('template-parts/card', 'standard_post'); ?>
								<?php endif; ?>

							<?php $post_count++; endwhile; wp_reset_postdata(); endif; ?>

							<!-- AJAX Loaded Posts -->
							
							<?php
							// Ajax load more the rest of the posts.
							echo do_shortcode('[ajax_load_more 
												button_label="<h3 class=\'spaced\'>View More</h3><i class=\'fal fa-long-arrow-down\'></i>"
												button_loading_label="<h3 class=\'spaced\'>Loading</h3><i class=\'fal fa-spinner\'></i>"
												transition_container_classes="post_grid alternating" 
												theme_repeater="card-standard_post.php" 
												posts_per_page="8"
												scroll="false" 
												offset="11"]'
											); 
							?>

						</div>

					</div>

				</div>
			
			</div>

		<?php endwhile; ?>

	</main>

<?php get_footer(); ?>
