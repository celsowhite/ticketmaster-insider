<?php
/*
Template Name: Minimaster Events
*/

get_header(); ?>

	<main class="main_wrapper">

        <?php while ( have_posts() ) : the_post(); ?>
        
            <?php 
            $sponsor = 'minimaster';
            include(locate_template('template-parts/slider-featured_minimaster_attractions.php'));
            ?>

			<div class="page_content">
                
                <div class="tm_section">
                    
                    <div class="container">

                        <div class="tm_leaderboard_ad_container">
                            <?php get_template_part('template-parts/component', 'leaderboard_ad_unit'); ?>
                        </div>
                        
                        <?php
                        // Output each of the Minimaster event terms and their posts.
                        $terms = get_terms( array(
                            'taxonomy'   => 'tm_minimaster_event_type',
                            'hide_empty' => false,
                        ));
                        foreach($terms as $term):
                        ?>

                            <div class="tm_content_container">
                                
                                <!-- Event Box Category Title -->
                                
                                <div class="title colorful">
                                    <h1><?php echo $term->name; ?></h1>
                                </div>

                                <!-- Event Box Carousel -->
                                
                                <div class="tm_content_container_body">
                                    
                                    <div class="tm_carousel_container">

                                        <div class="tm_carousel">
                                            <?php
                                            $events_loop_args = array (
                                                'post_type' => 'tm_minimaster_event', 
                                                'tax_query' => array(
                                                    array(
                                                        'taxonomy' => 'tm_minimaster_event_type',
                                                        'field'    => 'slug',
                                                        'terms'    => $term->slug,
                                                    ),
                                                ),
                                            );
                                            $events_loop = new WP_Query($events_loop_args);
                                            if ($events_loop -> have_posts()) : while ($events_loop -> have_posts()) : $events_loop -> the_post();
                                            ?>
                                                
                                                <?php get_template_part('template-parts/card', 'minimaster_event'); ?>

                                            <?php endwhile; wp_reset_postdata(); endif; ?>

                                        </div>

                                        <div class="tm_carousel_nav"></div>

                                    </div>

                                </div>

                            </div>

                        <?php endforeach; ?>

                    </div>

                </div>

			</div>
			
		<?php endwhile; ?>

	</main>

<?php get_footer(); ?>