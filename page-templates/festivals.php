<?php
/*
Template Name: Festivals
*/

get_header(); ?>

	<main class="main_wrapper">

		<?php while ( have_posts() ) : the_post(); ?>

			<?php 
			$slider_background_color = 'blue';
            $sponsor = 'festivals';
            include(locate_template('template-parts/slider-featured_posts.php'));
            ?>

			<div class="page_content">
				
				<div class="tm_section">
				
					<div class="container">
						
						<div class="tm_row">
							
							<div class="column_2_3">

								<div class="upcoming_festivals_container">
								
									<!-- Upcoming Festivals Header -->

									<header class="upcoming_festivals_header">

										<!-- Title/Search -->

										<div class="upcoming_festivals_header_top">
											<h1>Upcoming Festivals</h1>
											<form class="tm_search_form festival_search_form">
												<input type="search" name="search" placeholder="Artist">
												<button type="submit" value="" />
													<div class="search_icon">
														<span class="circle"></span>
														<span class="handle"></span>
													</div>
												</button>
											</form>
										</div>

										<!-- Festival Filter Location/Month -->

										<div class="upcoming_festivals_header_bottom">
											<?php get_template_part('template-parts/component', 'festival_filter'); ?>
										</div>
										
									</header>
									
									<!-- Upcoming Festivals Body -->

									<div class="upcoming_festivals_body">

										<!-- Header -->
										
										<div class="upcoming_festivals_results_header">
											<h1>Popular</h1>
											<i class="fal fa-times-circle festival_clear_filter"></i>
										</div>

										<!-- Results List -->

										<ul class="festival_list"></ul>

										<!-- Load More Button -->

										<div class="tm_load_more festival_list_load_more">
											<h3 class="spaced">View More</h3>
											<i class="fal fa-long-arrow-down"></i>
										</div>

										<!-- Loading Container -->

										<div class="upcoming_festivals_loading_container">
											<i class="fal fa-spinner"></i>
										</div>

									</div>
								
								</div>

							</div>

							<div class="column_1_3">

								<div class="tm_widget tm_ad_widget">
									<?php get_template_part('template-parts/component', 'rectangle_ad_unit'); ?>
								</div>

								<div class="tm_widget">
									<h1 class="widget_title">Festival Stories</h1>
									<?php
									$festival_stories_loop_args = array ('post_type' => 'post', 'posts_per_page' => 5, 'category_name' => 'music-festivals');
									$festival_stories_loop = new WP_Query($festival_stories_loop_args);
									if ($festival_stories_loop -> have_posts()) : while ($festival_stories_loop -> have_posts()) : $festival_stories_loop -> the_post();
									?>
										<?php get_template_part('template-parts/card', 'sidebar_post'); ?>	
									<?php endwhile; wp_reset_postdata(); endif; ?>
									<a class="link_with_arrow" href="<?php echo category_link_by_slug('music-festivals'); ?>">
										<h3 class="spaced">View More</h3>
										<i class="fal fa-long-arrow-right"></i>
									</a>
								</div>

							</div>

						</div>

					</div>

				</div>

			</div>
			
		<?php endwhile; ?>

	</main>

<?php get_footer(); ?>
