<?php
/**
 * The template for displaying search results pages.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package _s
 */

get_header(); ?>

	<main id="main post-<?php the_ID(); ?>" class="main_wrapper" role="main">

	 	<div class="search_header">
			<div class="small_container">
				<?php get_search_form(); ?>
				<h3 class="spaced">Here Is What We Found</h3>
			</div>
		</div>
		
		<?php
		$term = (isset($_GET['s'])) ? $_GET['s'] : '';
		?>
		<div class="container">

			<div class="tm_row">
				<div class="column_2_3">
					
					<?php if(have_posts()): ?>

						<?php 

						// Ajax Load More / Relevanssi
						// AJLM takes the query args and passes them through Relevanssi do_query(). Then returns it to the DOM.

						echo do_shortcode('[ajax_load_more 
												id="relevanssi"
												search="'. $term .'"
												post_type="post"
												button_label="<h3 class=\'spaced\'>View More</h3><i class=\'fal fa-long-arrow-down\'></i>"
												button_loading_label="<h3 class=\'spaced\'>Loading</h3><i class=\'fal fa-spinner\'></i>"
												theme_repeater="card-horizontal_post.php" 
												posts_per_page="10"
												scroll="false"]'
											);

						?>
					
					<?php else: ?>

						<h1>No results for "<?php echo $term; ?>"</h1>

					<?php endif; ?>

				</div>
				<div class="column_1_3">
					<?php get_template_part('template-parts/post', 'sidebars'); ?>
				</div>
			</div>

		</div>

	</main>

<?php get_sidebar(); ?>
<?php get_footer(); ?>
