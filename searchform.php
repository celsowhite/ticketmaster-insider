<form role="search" method="get" class="tm_search_form" action="<?php echo home_url( '/' ); ?>">
    <input type="search" placeholder="Search" value="<?php echo get_search_query() ?>" name="s" title="Search NYSS ..." />
    <button type="submit" value="→" />
        <div class="search_icon">
            <span class="circle"></span>
            <span class="handle"></span>
        </div>
    </button>
</form>