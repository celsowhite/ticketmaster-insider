<?php
/**
 * The template for displaying archive pages.
 */

get_header(); ?>

	<main id="main post-<?php the_ID(); ?>" class="main_wrapper" role="main">

		<?php 
		$cat = get_query_var('cat');
		$category = get_category ($cat);
		if ( have_posts() ) : ?>

			<?php $post_count = 1; while ( have_posts() ) : the_post(); ?>

				<?php 
				// First post is positioned in the hero of the page.
				if($post_count === 1): ?>
					
					<div class="hero_container">
						<div class="hero_image_container" style="background-image: url(<?php the_post_thumbnail_url('large') ?>);"></div>
						<div class="hero_content_container">
							<div class="hero_content_box_container">
								<div class="hero_content_box">
									<div class="hero_content">
										<h3 class="spaced">Events</h3>
										<h1><a href="<?php the_permalink(); ?>"><?php echo tm_title_trim(get_the_title()); ?></a></h1>
										<p><?php echo tm_excerpt_trim(get_the_excerpt()); ?></p>
									</div>
								</div>
							</div>
						</div>
					</div>

				<?php break; endif; ?>

			<?php $post_count++; endwhile; ?>

			<div class="container">

				<h1 class="inpage_header">Latest <?php echo $category->name; ?></h1>

				<?php 
				// Ajax load more the rest of the posts in this category.
				$button_label = '<h3>View More</h3><i class="fal fa-long-arrow-right"></i>';
				echo do_shortcode('[ajax_load_more 
									button_label="<h3 class=\'spaced\'>View More</h3><i class=\'fal fa-long-arrow-down\'></i>"
									button_loading_label="<h3 class=\'spaced\'>Loading</h3><i class=\'fal fa-spinner\'></i>"
									category="'.$category->slug.'" 
									transition_container_classes="post_grid alternating" 
									theme_repeater="card-standard_post.php" 
									posts_per_page="8"
									scroll="false" 
									offset="1"]'
								); 
				?>
			</div>

		<?php endif; ?>

	</main>

<?php get_footer(); ?>
