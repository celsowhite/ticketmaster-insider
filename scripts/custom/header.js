(function ($) {

	$(document).ready(function () {

		"use strict";

		/*================================= 
		Elements
		=================================*/

		const body = document.querySelector('body');

		const header = document.querySelector('.main_header');

		const currentMenuItem = document.querySelector('.main_navigation ul li.current-menu-item');

		const currentMenuParent = document.querySelector('.main_navigation ul li.current-menu-parent');

		const searchIcon = document.querySelector('.main_header .search_icon');

		/*================================= 
		Sub Menu
		=================================*/

		// Check if current menu item has a sub menu

		if(currentMenuItem) {
			if (currentMenuItem.classList.contains('menu-item-has-children')) {
				body.classList.add('page_with_subnav');
			}
		}

		if (currentMenuParent) {
			if (currentMenuParent.classList.contains('menu-item-has-children')) {
				body.classList.add('page_with_subnav');
			}
		}

		/*================================= 
		Search Icon
		=================================*/

		const searchOverlayInput = document.querySelector('.search_overlay input[type="search"]');

		// On click, transform search icon to close icon

		searchIcon.addEventListener('click', function() {
			
			// Toggle classes to open/close search

			this.classList.toggle('transformed');
			body.classList.toggle('search_open');

			// If opening the overlay then focus the input on the search field

			if(this.classList.contains('transformed')) {
				searchOverlayInput.focus();
			}

		});

		/*================================= 
		Fixed Navigation
		=================================*/

		// Set a variable for caching the scrollY in the window so we know if we are scrolling up or down.

		let lastScrollTop = 0;

		function transformHeader() {

			// Set the current scroll top

			const currentScrollTop = window.pageYOffset || document.documentElement.scrollTop;

			// If the current scroll is greater than the last then we are scrolling down.
			// Also only trigger the header transformation if scrolled 30 pixels from the top.

			if (currentScrollTop > lastScrollTop && lastScrollTop > 30) {
				header.classList.add('scrolled');
				setTimeout(() => header.classList.contains('scrolled') && header.classList.add('scrolled_active'), 500);
			}
			else if (currentScrollTop === 0) {
				header.classList.remove('scrolled', 'scrolled_active');
			}
			else {
				header.classList.remove('scrolled', 'scrolled_active');
			}

			// Cache the last scroll top value

			lastScrollTop = currentScrollTop;
		}

		window.addEventListener('scroll', transformHeader);

		/*================================= 
		MOBILE NAVIGATION REVEAL
		=================================*/

		const mobileMenuIcon = document.querySelector('.mobile_header .menu_icon');
		const mobileNavigation = document.querySelector('.mobile_navigation');

		mobileMenuIcon.addEventListener('click', () => {
			mobileMenuIcon.classList.toggle('open');
			mobileNavigation.classList.toggle('open');
		});

		/*================================= 
		MOBILE NAVIGATION SUBMENU REVEAL
		=================================*/

		const mobileMenuLink = document.querySelectorAll('.mobile_navigation li.menu-item-has-children > a');

		Array.from(mobileMenuLink).forEach((link) => {
			link.addEventListener('click', function(e) {

				e.preventDefault();

				// Save the containing <li>

				const containingListItem = link.closest('li');

				// Find the sub menu and open/close it

				const subMenu = containingListItem.querySelector('ul.sub-menu');
				subMenu.classList.toggle('open');
				
			})
		});

	});

})(jQuery);