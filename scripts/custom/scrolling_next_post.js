(function($) {

	$(document).ready(function() {

	"use strict";

		/*=================================
        SCROLLING NEXT POST
        =================================*/

        // Elements

        const singlePostBody = document.querySelector('.single_post_body');

        const singlePostContent = document.querySelector('.single_post_content');

        const singlePostSidebar = document.querySelector('.single_post_sidebar');

        const nextPostCard = document.querySelector('.next_post_card_container');

        // Show/Hide Next Post

        function nextPostTrigger() {

            // Current Scroll Top

            const currentScrollTop = window.pageYOffset || document.documentElement.scrollTop;

            // Single Post Content Positioning

            const bodyPositioning = singlePostBody.getBoundingClientRect();

            // If the content positioning hasn't been scrolled past and it's within the window viewport.
            // Then show the next post card.

            if (bodyPositioning.bottom > 0 && bodyPositioning.bottom <= (window.innerHeight + 50)) {
                nextPostCard.classList.remove('hidden');
            }

            // Else hide the next post card.

            else {
                nextPostCard.classList.add('hidden');
            }

        }

        if(nextPostCard) {

            window.addEventListener('scroll', debounce(function(){
                nextPostTrigger();
            }, 20));

            window.setTimeout(() => {
                const contentHeight = singlePostContent.offsetHeight;
                const sidebarHeight = singlePostSidebar.offsetHeight;
                if(contentHeight > sidebarHeight) {
                    nextPostCard.style.right = '-100px';
                }
                else {
                    nextPostCard.style.left = '0px';
                }
            }, 3000);

        }

	});

})(jQuery);