(function($) {

	$(document).ready(function() {

    "use strict";
    
        /*================================= 
		Overall Elements
		=================================*/

        const body = document.querySelector('body');

        // List

        const festivalList = document.querySelector('.festival_list');

        // Body

        const festivalBody = document.querySelector('.upcoming_festivals_body');

        // Search

        const festivalSearchForm = document.querySelector('.festival_search_form');

        const searchInput = document.querySelector('.festival_search_form input[name="search"]');

        // Filter

        const festivalFilterForm = document.querySelector('.festival_filter_form');

        const festivalFilterFormButton = document.querySelector('.festival_filter_form button');

        // Location Dropdown

        const festivalLocation = document.querySelector('.festival_location_dropdown');

        const festivalLocationText = document.querySelector('.festival_location_dropdown .tm_dropdown_selection');

        // Month Dropdown

        const festivalMonth = document.querySelector('.festival_month_dropdown');

        const festivalMonthText = document.querySelector('.festival_month_dropdown .tm_dropdown_selection');

        // Clear

        const festivalClearFilter = document.querySelector('.festival_clear_filter');

        // Load More

        const loadMore = document.querySelector('.festival_list_load_more');

        const maxItemsToShow = 3;

        // Results Header

        const resultsHeader = document.querySelector('.upcoming_festivals_results_header');

        const resultsTitle = document.querySelector('.upcoming_festivals_results_header h1');

        // No Results Text

        const noResultsText = "<p>No results found. Try your search again or check back soon - we're always adding new festivals!</p>"

        /*================================= 
		Helpers
        =================================*/

        // Show Load More Button

        function showLoadMoreButton() {
            loadMore.classList.remove('done');
        }

        // Hide Load More Button

        function hideLoadMoreButton() {
            loadMore.classList.add('done');
        }

        // Hide Festival Items
        // Only shows a certain amount of returned festival events when they are loaded into the DOM.

        function hideFestivalItems() {

            // Grab the list items in the DOM

            const festivalListItems = document.querySelectorAll('.festival_list .inventory_widget_container');
            const itemCount = festivalListItems.length;

            // If there are more items in our list than our max allowed then dynamically hide the extras

            if(itemCount > maxItemsToShow) {

                // Loop through all of the list items

                Array.from(festivalListItems).forEach((listItem, index) => {

                    // If this item is beyond our max allowed items then hide it by default

                    if (index >= maxItemsToShow) {
                        listItem.classList.add('hidden');
                    }

                });

            }

            // Else hide the load more button.

            else {

                hideLoadMoreButton();

            }

        }

        // Render Popular Festivals

        function renderPopularFestivals() {

            festivalBody.classList.add('loading');

            // Show the popular festivals list.

            $.ajax({
                type: "post",
                url: wpUrls.ajax_url,
                data: {
                    action: 'render_popular_festivals'
                }
            }).done(function (html) {
                festivalBody.classList.remove('loading');
                resultsHeader.classList.remove('filtered');
                resultsTitle.innerHTML = 'Popular';
                if (html) {
                    festivalList.innerHTML = html;
                    showLoadMoreButton();
                    hideFestivalItems();
                }
                else {
                    festivalList.innerHTML = noResultsText;
                    hideLoadMoreButton();
                }
            });

        }

        if(festivalList) {

            /*================================= 
            Show Popular Festivals on Page Load
            =================================*/
            
            renderPopularFestivals();

            /*================================= 
            Festival Search
            =================================*/
            
            // On submit of the search ajax request our internal php function
            // The function finds relevant festivals and returns them.

            festivalSearchForm.addEventListener('submit', function (e) {

                e.preventDefault();

                const searchKeyword = searchInput.value;

                if (searchKeyword) {

                    festivalBody.classList.add('loading');
                    
                    $.ajax({
                        type: "post",
                        url: wpUrls.ajax_url,
                        data: {
                            action: 'festival_search',
                            keyword: searchKeyword
                        }
                    }).done(function (html) {
                        festivalBody.classList.remove('loading');
                        resultsHeader.classList.add('filtered');
                        resultsTitle.innerHTML = 'Results';
                        // If content was returned then populate the festivals list on the page.
                        if (html) {
                            festivalList.innerHTML = html;
                            showLoadMoreButton();
                            hideFestivalItems();
                        }
                        // Else no results were found.
                        else {
                            festivalList.innerHTML = noResultsText;
                            hideLoadMoreButton();
                        }
                    });
                }

            });

            /*================================= 
            Festival Filter
            =================================*/
            
            festivalFilterFormButton.addEventListener('click', function (e) {

                e.preventDefault();

                festivalBody.classList.add('loading');

                // Save the location and month values.

                const location = festivalLocation.dataset.value;

                const locationType = festivalLocation.dataset.type;

                const month = festivalMonth.dataset.value;

                // By default set start time to today and end time to the end of the year.

                let startDate = moment().format('YYYY-MM-DD');
                let endDate = moment().endOf('year').format('YYYY-MM-DD');

                // If month is selected then save the start and end date of the month.

                if(month) {
                    startDate = moment().month(month).startOf('month').format('YYYY-MM-DD');
                    endDate = moment().month(month).endOf('month').format('YYYY-MM-DD');
                }

                $.ajax({
                    type: "post",
                    url: wpUrls.ajax_url,
                    data: {
                        action: 'festival_filter',
                        location: location,
                        locationType: locationType,
                        startDate: startDate,
                        endDate: endDate
                    }
                }).done(function (html) {
                    festivalBody.classList.remove('loading');
                    resultsHeader.classList.add('filtered');
                    resultsTitle.innerHTML = 'Results';
                    if (html) {
                        festivalList.innerHTML = html;
                        showLoadMoreButton();
                        hideFestivalItems();
                    }
                    else {
                        festivalList.innerHTML = noResultsText;
                        hideLoadMoreButton()
                    }
                });

            });
                
            /*================================= 
            Festival Clear Filter
            =================================*/
            
            festivalClearFilter.addEventListener('click', () => {
                
                // Show Popular Festivals
                
                renderPopularFestivals();

                // Reset Search Input

                searchInput.value = '';
                
                // Reset Location Dropdown
                
                festivalLocation.dataset.type = '';
                
                festivalLocation.dataset.value = '';
                
                festivalLocationText.innerHTML = 'Location';
                
                // Reset Month Dropdown
                
                festivalMonth.dataset.value = '';
                
                festivalMonthText.innerHTML = 'Month';

            });

            /*================================= 
            Load More
            =================================*/

            // On click, visually show more festivals in the list.

            loadMore.addEventListener('click', () => {

                // Grab the currently hidden items in the list

                const hiddenFestivalListItems = document.querySelectorAll('.festival_list .inventory_widget_container.hidden');
                const itemCount = hiddenFestivalListItems.length;

                // Async loop through each of the items

                async.forEachOf(hiddenFestivalListItems, function (listItem, index, callback) {
                    
                    // If the item is within our designated amount to show then show it.

                    if (index < maxItemsToShow) {
                        // Add the 'visually_hidden' class so the item remains transparent.
                        // Remove the 'hidden' class so we display the element block.
                        listItem.classList.add('visually_hidden');
                        listItem.classList.remove('hidden');
                    }

                    // Wait 1ms until we visually show the item.
                    setTimeout(() => {
                        listItem.classList.remove('visually_hidden');
                    }, 100);

                    callback();

                }, function (err) {
                    
                    // After showing all of the items then recalculate how many hidden items we still have left.

                    const updatedHiddenFestivalListItems = document.querySelectorAll('.festival_list .inventory_widget_container.hidden');

                    // If no more hidden items then hide the load more button.

                    if(updatedHiddenFestivalListItems.length === 0){
                        loadMore.classList.add('done');
                    }

                });

            });

        }

	});

})(jQuery);