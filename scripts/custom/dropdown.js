(function($) {

	$(document).ready(function() {

	"use strict";

		/*================================= 
		Custom Dropdown
		=================================*/

        // Find all of the custom dropdowns on the page.

        const tmDropdowns = document.querySelectorAll('.tm_dropdown');
        
        Array.from(tmDropdowns).forEach((dropdown) => {

            // Add a click event listener to each.

            dropdown.addEventListener('click', (e) => {

                // Save the selected text container

                const selectedText = dropdown.querySelector('.tm_dropdown_selection');

                // Toggle the dropdown open/close

                dropdown.classList.toggle('open');

                if (e.target.nodeName === 'LI'){

                    // Grab the data attributes of the selected item and set them on the parent dropdown element.
                    
                    for(let key in e.target.dataset) {
                        dropdown.dataset[key] = e.target.dataset[key];
                    }

                    // Set the selected label

                    selectedText.textContent = e.target.textContent;

                };

            });

        });

	});

})(jQuery);