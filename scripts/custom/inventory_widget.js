(function($) {

	$(document).ready(function() {

    "use strict"; 

        /*---------------------------------
        Variables
        ---------------------------------*/

        const googleApiKey = 'AIzaSyCJCXtK2WCgHzLhcX1Z_BCg8IV-ZvumAKk';
        const discoveryApiKey = 'FvYYASSbYvnjisVLWo5ZmUhfUgMVHRpF';

        /*---------------------------------
        Helpers
        ---------------------------------*/

        /**
        * Create a Fetch Request URL
        *
        * @param   string  baseURL  -  The base of the libaries API request url.
        * @param   object  params   -  Object of query parameters.
        *
        * @return  string  Returns the full url that should be fetched.
        */

        function createFetchURL(baseURL, params) {
            return baseURL + Object.keys(params).map(key => key + '=' + params[key]).join('&');
        }

        /**
        * Get Geolocation Guide
        *
        * @param   string  browser  -  Browser name.
        *
        * @return  string  Returns the url of the browser guide for how to enable geolocation.
        */

        function getGeolocationGuide(browser) {
            
            let browserGuideLink;

            if (bowser.name === 'Chrome') {
                browserGuideLink = 'https://support.google.com/chrome/answer/142065?hl=en';
            }
            else if (bowser.name === 'Safari') {
                browserGuideLink = 'https://support.apple.com/en-us/HT204690';
            }
            else if (bowser.name === 'Firefox') {
                browserGuideLink = 'https://support.mozilla.org/en-US/questions/988163';
            }
            else {
                browserGuideLink = ''
            }

            return browserGuideLink;

        }

        /*---------------------------------
        Get Users Location Info
        ---
        Uses latitude and longitude to reverse geocode the users location.
        ---------------------------------*/

        /**
        * Get Users Location Info
        *
        * @param   int   latitude  -  Latitude value.
        * @param   int   longitude  -  Longitude value.
        *
        * @return  obj   Resolves with details about the users location. City, State, Country.
        */

        function getUsersLocationInfo(latitude, longitude) {

            return new Promise(function(resolve, reject) {

                // Google Maps API Fetch Request

                const googleMapsURL = `https://maps.googleapis.com/maps/api/geocode/json?latlng=${latitude},${longitude}&key=${googleApiKey}`;

                fetch(googleMapsURL, {
                    method: 'get'
                }).then(function (response) {
                    return response.json();
                }).then(function (data) {
                    
                    // Save the results and an empty object to populate the location info.

                    const results = data.results;
                    let usersLocationInfo = {};

                    // Results are structured in a bit of a strange way so need to loop through the results and pull the parameters we need.
                    // Results are returned with a few address types. The list goes from most specific to least specific. Since we don't need the exact user address we grab the 2nd item in the list.
                    // The first result may be so specific that it is wrong.

                    for (var i = 0; i < results[1].address_components.length; i++) {
                       
                        var component = results[1].address_components[i];
                        
                        // City

                        if (component.types.includes('sublocality') || component.types.includes('locality')) {
                            usersLocationInfo.city = component.long_name;
                        }

                        // State

                        else if (component.types.includes('administrative_area_level_1')) {
                            usersLocationInfo.state = component.short_name;
                        }

                        // Country

                        else if (component.types.includes('country')) {
                            usersLocationInfo.country = component.long_name;
                            usersLocationInfo.registered_country_iso_code = component.short_name;
                        }

                    };

                    resolve(usersLocationInfo);

                });

            });
            
        }
        
        /**
        * Fetch Ticketmaster Event Data
        *
        * @param   string   url  -  Ticketmaster API url to fetch.
        *
        * @return  obj      Resolves with a list of events from the API.
        */

        function fetchEventData(url) {

            return new Promise(function(resolve, reject) {

                fetch(url, {
                    method: 'get'
                }).then(function (response) {
                    return response.json();
                }).then(function (data) {

                    // Check if events were returned given this query.

                    if(data._embedded) {

                        // Save the events

                        const events = data._embedded.events.sort(function (a, b) {
                            // Sort by ascending start date. Events won't always be sent in order of date. 
                            // In the case of geolocation, events will sometimes sent in order of relevancy.
                            return new Date(a.dates.start.localDate) - new Date(b.dates.start.localDate);
                        });

                        // Return the sorted events list to the user.

                        resolve(events);

                    }
                    else {
                        resolve();
                    }

                });

            });

        }

        function getAttractionImage(attractionId) {

            return new Promise(function(resolve, reject) {

                const queryURL = `https://app.ticketmaster.com/discovery/v2/attractions/${attractionId}?apikey=${discoveryApiKey}`;

                fetch(queryURL, {
                    method: 'get'
                }).then(function (response) {
                    return response.json();
                }).then(function (data) {
                    resolve(data.images[0].url);
                });

            });

        }

        function getVenueImage(venueId) {

            return new Promise(function (resolve, reject) {

                const queryURL = `https://app.ticketmaster.com/discovery/v2/venues/${venueId}?apikey=${discoveryApiKey}`;

                fetch(queryURL, {
                    method: 'get'
                }).then(function (response) {
                    return response.json();
                }).then(function (data) {
                    resolve(data.images[0].url);
                });

            });

        }

        /**
        * Render Inventory Widget
        *
        * @param   obj     widgetContainer       -  The widget containers DOM element.
        * @param   obj     inventoryAttributes   -  Object of attributes we want to query from the Discovery API.
        * @param   bool    geolocationQuery      -  True/False if this is a geolocation based query.
        * @param   obj     location              -  Users location passed through as a lat/long object. Null if not a geolocation query.
        *
        * @return  Renders the list of events in the widgets container.
        */

        function renderInventoryWidget(widgetContainer, inventoryAttributes, geolocationQuery, location) {

            // DOM Elements

            const widgetElement = widgetContainer.querySelector('.inventory_widget_events');

            const widgetSubTitle = widgetContainer.querySelector('.inventory_widget_sub_title');

            const widgetHeaderImage = widgetContainer.querySelector('.inventory_widget_header_image');

            // Set the URL and default query parameters

            const baseURL = 'https://app.ticketmaster.com/discovery/v2/events.json?';

            const queryParams = {
                apikey: discoveryApiKey,
                size: 20,
                sort: "date,asc",
                startDateTime: moment().format()
            }

            // Loop through the inventory attributes.

            Object.keys(inventoryAttributes).forEach(function (key, index) {

                // If an attribute is set then add it to our query params.

                if(inventoryAttributes[key]) {
                    queryParams[key] = inventoryAttributes[key];
                }

            });

            // If location is passed to this function then we can query the Discovery API for events based on the users location.

            if (geolocationQuery) {

                if (location) {

                    const latitude = location.coords.latitude;

                    const longitude = location.coords.longitude;

                    // Get Users Location Information (City, State, etc)

                    getUsersLocationInfo(latitude, longitude).then((usersLocationInfo) => {
                        widgetSubTitle.innerHTML = `<p>Near: ${usersLocationInfo.city}, ${usersLocationInfo.state}</p>`;
                    });

                    // Add location details to our query params object.

                    queryParams.geoPoint = Geohash.encode(latitude, longitude, 9);

                    queryParams.radius = 50;

                    queryParams.unit = 'miles';

                    // Indicate that we want to sort data from the API in order of relevance.

                    queryParams.sort = "relevance,desc";

                }
                else {

                    // If this was a geolocation query but we weren't able to access the users location info,
                    // then add a message to the DOM instructing the user on how to enable location services.

                    const browserGuideLink = getGeolocationGuide(bowser.name);
                    let geolocationHelpText;

                    if (browserGuideLink) {
                        geolocationHelpText = `We weren't able to detect your current location. Please enable geolocation in your browser so we can show events nearby you.`;
                    }
                    else {
                        geolocationHelpText = `Please enable geolocation in your browser so we can show events nearby you.`;
                    }

                    widgetSubTitle.innerHTML =  `<div class="title_icon_container" data-balloon="${geolocationHelpText}" data-balloon-pos="right" data-balloon-length="large">
                                                    <i class="far fa-question-circle"></i>
                                                </div >`
                }

            }

            // Save Widget Header Image

            if (inventoryAttributes.attractionId) {

                getAttractionImage(inventoryAttributes.attractionId).then((attractionImageUrl) => {

                    const urlString = `url('${attractionImageUrl}')`;
                    widgetHeaderImage.style.backgroundImage = urlString;

                });

            }
            else if(inventoryAttributes.venueId) {
                
                getVenueImage(inventoryAttributes.venueId).then((venueImageUrl) => {

                    const urlString = `url('${venueImageUrl}')`;
                    widgetHeaderImage.style.backgroundImage = urlString;

                });

            }

            else {

                widgetHeaderImage.style.display = 'none';

            }

            const apiURL = createFetchURL(baseURL, queryParams);

            // Fetch the event data from the Discovery API

            fetchEventData(apiURL).then((events) => {

                // Add the loaded class to the container so we change the visual display of the widget.

                widgetContainer.classList.add('events_loaded');

                if (events) {

                    // Show the events in the DOM

                    widgetElement.innerHTML = events.map((event) => {

                        // Get event location info

                        let eventLocationInfo = '';

                        // Helpers to return event info

                        function hasStartTime(startTime) {
                            return startTime ? `<p class="time">${moment(startTime, 'HH:mm').format('LT')}</p>` : '';
                        }

                        function hasVenueName(venueName) {
                            return venueName ? `- ${venueName}`: '';
                        }

                        if (event._embedded.venues[0].city && event._embedded.venues[0].state) {
                            eventLocationInfo = `${event._embedded.venues[0].city.name}, ${event._embedded.venues[0].state.stateCode}`;
                        }
                        else if (event._embedded.venues[0].city && event._embedded.venues[0].country) {
                            eventLocationInfo = `${event._embedded.venues[0].city.name}, ${event._embedded.venues[0].country.name}`;
                        }

                        // Create a string of the event data

                        return `
                            <li class="inventory_widget_event">
                                <div class="event_info_container">
                                    <div class="date_time_container">
                                        <p class="date">${moment(event.dates.start.localDate).format('ddd, MMM D')}</p>
                                        ${hasStartTime(event.dates.start.localTime)}
                                    </div>
                                    <div class="name_location_container">
                                        <p class="event_name"><a href="${event.url}" target="_blank">${event.name}</a></p>
                                        <p class="location">${eventLocationInfo} ${hasVenueName(event._embedded.venues[0].name)}</p>
                                    </div>
                                </div>
                                <div class="tickets_button_container">
                                    <a class="tm_button small" href="${event.url}" target="_blank">Find Tickets</a>
                                </div>
                            </li>
                        `;

                    }).join('');

                }
                else {

                    widgetElement.innerHTML = '<p class="no_events">No upcoming events.</p>'

                }

            });

        }

        /*---------------------------------
        Inventory Widget Initialization
        ---------------------------------*/

        const inventoryWidgets = document.querySelectorAll('.inventory_widget_container.dynamically_populated');

        // If there are inventory widgets on the page then render them.

        if (inventoryWidgets) {

            Array.from(inventoryWidgets).forEach((widget) => {
                
                const inventoryAttributes = {
                    'attractionId': widget.dataset.attractionId,
                    'classificationId': widget.dataset.classificationId,
                    'countryCode': widget.dataset.countryCode,
                    'dmaId': widget.dataset.dmaId,
                    'endDateTime': widget.dataset.endDateTime,
                    'keyword': widget.dataset.keyword,
                    'marketId': widget.dataset.marketId,
                    'size': widget.dataset.size,
                    'startDateTime': widget.dataset.startDateTime,
                    'venueId': widget.dataset.venueId
                }

                // If querying by geolocation and geolocation is available in the users browser then get the users current location.

                if (widget.dataset.geolocation && navigator.geolocation) {
                                        
                    navigator.geolocation.getCurrentPosition(function (location) {

                        renderInventoryWidget(widget, inventoryAttributes, true, location);

                    }, function (error) {

                        // If error is returned then the user likely denied geolocation.
                        // Render the widget without taking into account the users current location.

                        renderInventoryWidget(widget, inventoryAttributes, true, null);

                    }, { 
                        // Set timeout because js geolocation will occasionally run infinitely and not return users location.
                        timeout: 10000 
                    });

                }

                // Else then fetch upcoming event data without taking into account the users current location.

                else {
                    renderInventoryWidget(widget, inventoryAttributes, false, null);
                }

            });

        }

	});

})(jQuery);