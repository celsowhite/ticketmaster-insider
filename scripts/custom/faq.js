(function($) {

	$(document).ready(function() {

	"use strict";

        /*================================= 
        New FAQ Expand
        =================================*/

        // Find all of the FAQ's on the page.

        const tmFaq = document.querySelectorAll('.tm_faq');

        Array.from(tmFaq).forEach((faq) => {

            // Add a click event listener to each.

            faq.addEventListener('click', (e) => {

                // Toggle the open state on click.

                faq.classList.toggle('open');

            });

        });

		/*================================= 
		Legacy FAQ Expand
		=================================*/

        // Find all of the legacy FAQ's on the page.

        const bqFaq = document.querySelectorAll('.bq_faq');
        
        Array.from(bqFaq).forEach((faq) => {

            // Add a click event listener to each.

            faq.addEventListener('click', (e) => {

                // Toggle the open state on click.
                
                faq.classList.toggle('open');

            });

        });

	});

})(jQuery);