(function($) {

	$(document).ready(function() {

        "use strict"; 

        /*================================= 
		Hero Slider
		=================================*/

		$('.hero_slider_images').flexslider({
	    	animation: "slide",
	    	controlNav: false,
	    	directionNav: false,
            smoothHeight: false,
            before: function (slider) {
                $('.hero_slider_content').flexslider(slider.animatingTo);
            }
        });
        
        $('.hero_slider_content').flexslider({
            animation: "fade",
            controlNav: true,
            controlsContainer: $(".tm_flexslider_controls_container"),
            customDirectionNav: $(".tm_flexslider_navigation a"),
            sync: '.hero_slider_images'
        });

        /*================================= 
		Gallery Slider
        =================================*/
        
        $('.tm_gallery_slider').flexslider({
            animation: "fade",
            controlNav: true,
            directionNav: false,
            manualControls: 'ul.gallery_slider_thumbnails li'
        });

        /*================================= 
		Slick Carousel
        =================================*/
        
        $('.tm_carousel_container').each(function () { 

            const carousel = $(this).find('.tm_carousel');
            const carouselNav = $(this).find('.tm_carousel_nav');

            carousel.slick({
                dots: true,
                speed: 300,
                prevArrow: '<button type="button" class="slick-prev"><i class="fal fa-long-arrow-left"></i></button>',
                nextArrow: '<button type="button" class="slick-next"><i class="fal fa-long-arrow-right"></i></button>',
                appendArrows: carouselNav,
                appendDots: carouselNav,
                slidesToShow: 5,
                slidesToScroll: 5,
                responsive: [
                    {
                        breakpoint: 1024,
                        settings: {
                            centerMode: true,
                            centerPadding: '20px',
                            slidesToShow: 3
                        }
                    },
                    {
                        breakpoint: 568,
                        settings: {
                            centerMode: true,
                            centerPadding: '0px',
                            slidesToShow: 1
                        }
                    }
                ]
            });

        });
        
	});

})(jQuery);