(function($) {

	$(document).ready(function() {

        "use strict";
        
        /*================================= 
        MAGNIFIC
        http://dimsemenov.com/plugins/magnific-popup/documentation.html
        =================================*/

        $('.wp_custom_gallery').each(function () {
                $(this).magnificPopup({
                        delegate: 'a',
                        type: 'image',
                        titleSrc: 'title',
                        // Class that is added to popup wrapper and background
                        // make it unique to apply your CSS animations just to this exact popup
                        mainClass: 'mfp-fade',
                        gallery: {
                                enabled: true,
                                arrowMarkup: '<button title="%title%" type="button" class="lightbox_arrow lightbox_arrow_%dir%"></button>', // markup of an arrow button
                                tPrev: 'Previous (Left arrow key)', // title for left button
                                tNext: 'Next (Right arrow key)', // title for right button
                                tCounter: '<span class="mfp-counter">%curr% of %total%</span>' // markup of counter
                        }
                });
        });

        /*================================= 
        FITVIDS
        =================================*/

        // Wrap All Iframes with 'video_embed' for responsive videos

        $('iframe[src*="youtube.com"], iframe[src*="vimeo.com"]').wrap("<div class='video_embed'/>");

        // Target div for fitVids

        $(".video_embed").fitVids();
        
	});

})(jQuery);