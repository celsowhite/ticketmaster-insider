(function($) {

	$(document).ready(function() {

	"use strict";

        /*================================= 
		Elements
		=================================*/

        const modalTriggers = document.querySelectorAll('.modal_trigger');

        const modalClose = document.querySelectorAll('.modal_close');

        /*================================= 
		Helpers
        =================================*/

        // Load Lineup Modal

        function loadLineupModal(modalTrigger, modalContainer) {

            // Elements

            const modalTitleElement = modalContainer.querySelector('h1.modal_title');
            const modalBodyElement = modalContainer.querySelector('.modal_body');

            // Data

            const modalTitle = modalTrigger.dataset.title;
            const lineup = modalTrigger.dataset.lineup;
            const artists = lineup.split(', ').map((artist) => {
                return `<li>${artist}</li>`
            }).join('');


            // Dynamically set the content

            modalTitleElement.innerHTML = modalTitle;
            
            modalBodyElement.innerHTML = `<ul class="modal_lineup_list">${artists}</ul>`;

        }
        
        // Load Modal Video
        // If opening a video modal then add its video attributes and trigger the video to play.

        function loadModalVideo(modalTrigger, modalContainer) {

            // Elements

            const modalPlyrElement = modalContainer.querySelector('.tm_plyr_video');
            const videoType = modalTrigger.dataset.videoType;
            const videoURL = modalTrigger.dataset.videoUrl;

            // Dynamically set the Plyr DOM attributes

            modalPlyrElement.dataset.plyrProvider = videoType;
            modalPlyrElement.dataset.plyrEmbedId = videoURL;

            // Setup the plyr instance

            const modalPlyrInstance = new Plyr('.video_modal .tm_plyr_video');

            // Listen for when plyr is ready then trigger the instance to play

            modalPlyrInstance.on('ready', function (event) {
                const instance = event.detail.plyr;
                instance.play();
            });

        }

        /*================================= 
		Modal Open
		=================================*/

        document.querySelector("body").addEventListener("click", function (e) {

            // Using event delegation to open a modal.

            if (e.target && e.target.matches(".modal_trigger")) {

                e.preventDefault();

                // Elements

                const modalTrigger = e.target;
                const modalName = modalTrigger.dataset.modal;
                const modalContainer = document.querySelector('.' + modalName);

                // Open the modal

                modalContainer.classList.add('open');

                // Video Modal
                
                if (modalName === 'video_modal') {

                    loadModalVideo(modalTrigger, modalContainer);
                
                }

                // Lineup Modal

                if(modalName === 'lineup_modal') {
                    loadLineupModal(modalTrigger, modalContainer);
                }

            }

        });

        /*================================= 
		Modal Close
		=================================*/

        Array.from(modalClose).forEach(closeTrigger => {

            closeTrigger.addEventListener('click', function() {

                // Elements

                const modalContainer = this.closest('.modal');
                const modalContent = modalContainer.querySelector('.modal_content');

                // Close the modal
                
                modalContainer.classList.remove('open');

                // If closing a video modal then reset the inner DOM contents.

                if(modalContainer.classList.contains('video_modal')) {
                    modalContent.innerHTML = '<div class="tm_plyr_video" data-plyr-provider="" data-plyr-embed-id=""></div>';
                }

            })

        });

	});

})(jQuery);