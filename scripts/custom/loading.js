(function($) {

	$(document).ready(function() {

	"use strict";

		/*================================= 
		Page Load
		=================================*/

        const body = document.querySelector('body');
        
        setTimeout(() => {
            body.classList.add('loaded');
        }, 1000);

	});

})(jQuery);