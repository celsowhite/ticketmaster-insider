 <?php
/*-------------------------------------
Festival Event Card
---
Shown in Festival event carousels.
-------------------------------------*/
?>

<a class="event_card" href="<?php the_field('event_tickets_url'); ?>" target="_blank">
    <div class="event_card_image_container">
        <div class="event_card_image" style="background-image:url('<?php the_field('event_image'); ?>')"></div>
    </div>
    <div class="event_card_content">
        <p class="event_date"><?php echo tm_event_date_format(get_field('event_start_date')); ?></p>
        <div class="event_name_location">
            <p class="event_name"><?php the_title(); ?></p>
            <p><?php the_field('event_venue_city'); ?>, <?php the_field('event_venue_state'); ?></p>
        </div>
    </div>
</a>