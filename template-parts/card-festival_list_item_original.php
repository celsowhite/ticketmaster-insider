<?php

/*-------------------------------------
Festival List Item Card
---
Outputs a single festival card.
-------------------------------------*/

if(!isset($inventory_widget_type)) {
    $inventory_widget_type = 'large';
}

?>

<li class="inventory_widget_item <?php echo $inventory_widget_type; ?>">

    <!-- Image -->

    <div class="image" style="background-image: url('<?php the_field('event_image') ?>')"></div>
    
    <!-- Info -->

    <div class="info">
        <p class="event_name"><?php the_title(); ?></p>
        <p class="event_date"><?php echo tm_event_date_format(get_field('event_start_date')); ?></p>
        <p class="event_venue"><?php the_field('event_venue_name') ?></p>
        <p class="event_location"><?php the_field('event_venue_city'); ?>, <?php the_field('event_venue_state'); ?></p> 
        <?php if(get_field('event_attractions')): ?>
            <p class="event_lineup"><a class="modal_trigger" data-modal="lineup_modal" data-title="<?php the_title(); ?>" data-lineup="<?php the_field('event_attractions'); ?>">Lineup</a></p>
        <?php endif; ?>
    </div>

    <!-- Tickets Button -->

    <div class="button_container">

        <?php 
        // Variables
        $tickets_url = get_field('event_tickets_url');
        $tickets_now_disclaimer_popup = 'While Ticketmaster does not sell tickets to this event, you can still get great resale tickets at our partner site TicketsNow. Please note resale prices often exceed face value.'; 
        $default_disclaimer_popup = "You are leaving the Ticketmaster website and linking to another ticket seller’s website.";
        ?>

        <?php 
        // Ticketmaster
        if(strpos($tickets_url,'ticketmaster.com')): ?>
            <a class="tm_button" href="<?php the_field('event_tickets_url') ?>" target="_blank">Find Tickets</a>
        <?php 
        // Ticketsnow
        elseif(strpos($tickets_url,'ticketsnow.com')): ?>
            <a class="tm_button" href="<?php the_field('event_tickets_url') ?>" target="_blank" data-balloon="<?php echo $tickets_now_disclaimer_popup; ?>" data-balloon-pos="up" data-balloon-length="large">Find Tickets</a>
            <div class="button_subtext">
                <p>On TicketsNow</p>
                <p class="disclaimer">Resale prices often exceed face value.</p>
            </div>
        <?php 
        // Livenation
        elseif(strpos($tickets_url,'livenation.com')): ?>
            <a class="tm_button" href="<?php the_field('event_tickets_url') ?>" target="_blank">Find Tickets</a>
            <div class="button_subtext">
                <p>On Live Nation</p>
            </div>
        <?php 
        // Ticketweb
        elseif(strpos($tickets_url,'ticketweb.com')): ?>
            <a class="tm_button" href="<?php the_field('event_tickets_url') ?>" target="_blank" data-balloon="<?php echo $default_disclaimer_popup; ?>" data-balloon-pos="up" data-balloon-length="large">Find Tickets</a>
            <div class="button_subtext">
                <p>On TicketWeb</p>
            </div>
        <?php 
        // Partner Site
        else: ?>
            <a class="tm_button" href="<?php the_field('event_tickets_url') ?>" target="_blank" data-balloon="<?php echo $default_disclaimer_popup; ?>" data-balloon-pos="up" data-balloon-length="large">Find Tickets</a>
            <div class="button_subtext">
                <p>On Partner Site</p>
            </div>
        <?php endif; ?>

    </div>

</li>