<ul class="tm_social_share">
    <li class="fb_share" data-url="<?php the_permalink(); ?>"><i class="fab fa-facebook-f"></i></li>
    <li class="twitter_share" data-url="<?php the_permalink(); ?>" data-text="<?php echo tm_title_trim(get_the_title()); ?>" data-via="Ticketmaster"><i class="fab fa-twitter"></i></li>
</ul>