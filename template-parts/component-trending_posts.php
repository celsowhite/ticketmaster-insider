<h1 class="inpage_header color_pink">Trending</h1>

    <div class="tm_row trending_grid">

        <?php 
        $trending_tags = get_field('trending_tags', 'option');
        if( $trending_tags ): ?>

            <?php foreach( $trending_tags as $tag ): ?>

                <div class="column_1_3">

                    <div class="sidebar_posts_widget">

                        <header class="sidebar_posts_widget_header">
                            <ul class="tm_pills pink centered">
                                <li><a href="<?php echo get_term_link( $tag ); ?>">#<?php echo $tag->name; ?></a></li>
                            </ul>
                        </header>

                        <?php
                        $trending_loop_args = array (
                            'post_type' => 'post', 
                            'posts_per_page' => 3,
                            'tag__in' => array($tag->term_id)
                        );
                        $trending_loop = new WP_Query($trending_loop_args);
                        if ($trending_loop -> have_posts()) : while ($trending_loop -> have_posts()) : $trending_loop -> the_post();
                        ?>
                            <?php get_template_part('template-parts/card', 'sidebar_post'); ?>
                        <?php endwhile; wp_reset_postdata(); endif; ?>
                        
                        <?php
                        /*
                        <a class="link_with_arrow" href="<?php echo get_term_link($tag); ?>">
                            <h3 class="spaced">View More</h3>
                            <i class="fal fa-long-arrow-right"></i>
                        </a>
                        */
                        ?>

                    </div>

                </div>

            <?php endforeach; ?>

        <?php endif; ?>

    </div>