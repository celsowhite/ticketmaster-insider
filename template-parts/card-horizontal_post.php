<div class="horizontal_post_card">

    <!-- Image -->

    <div class="horizontal_post_card_image">
        <div class="horizontal_post_card_image_block" style="background-image: url('<?php echo tm_get_post_thumbnail($post->ID, 'large'); ?>')"></div>
        <a href="<?php the_permalink(); ?>" class="post_card_image_link"></a>
        <div class="horizontal_post_card_category">
            <h3 class="category_name"><?php echo get_the_category()[0]->cat_name; ?></h3>
        </div>
    </div>

    <!-- Content -->

    <div class="horizontal_post_card_content">
        <div class="horizontal_post_card_meta">
            <p class="meta">
                <?php echo get_the_date('M j, Y') ?>
            </p>
        </div>
        <h1><a href="<?php the_permalink(); ?>"><?php echo tm_title_trim(get_the_title()); ?></a></h1>
        <p class="excerpt"><?php echo tm_excerpt_trim(get_the_excerpt()); ?></p>
    </div>
    
</div>