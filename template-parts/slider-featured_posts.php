<?php

// Featured Posts Slider
// Pulls featured posts set via ACF.

if(!isset($slider_background_color)) {
    $slider_background_color = 'white';
}

if($slider_background_color === 'colorful' || $slider_background_color === 'blue') {
    $nav_color_scheme = 'white';
}

?>

<div class="hero_container <?php echo $slider_background_color; ?>">

    <?php 

    // Check if we are on a tag page before setting the posts. On tag pages need to pass an extra parameter
    // to ACF's get_field function.

    if(is_tag() || is_category()){
        $term_object = get_queried_object();
        $posts = get_field('featured_posts', $term_object);
    }
    else {
        $posts = get_field('featured_posts'); 
    }
    if($posts):
         
    ?>

         <!-- Sponsor -->

        <?php if(isset($sponsor)): ?>
            <div class="hero_sponsor">
                <h2>Presented By</h2>
                <?php 
                $sponsor_logo_field = $sponsor . '_sponsor_logo'; 
                $sponsor_link_field = $sponsor . '_sponsor_link';
                $sponsor_link_object = get_field($sponsor_link_field, 'option');
                if($sponsor_link_object):
                $sponsor_link_url = $sponsor_link_object['url'];
                $sponsor_link_target = $sponsor_link_object['target'];
                ?>
                    <a class="logo" href="<?php echo $sponsor_link_url; ?>" target="<?php echo $sponsor_link_target; ?>">
                        <img src="<?php echo get_field($sponsor_logo_field, 'option'); ?>" />
                    </a>
                <?php else: ?>
                    <div class="logo">
                        <img src="<?php echo get_field($sponsor_logo_field, 'option'); ?>" />
                    </div>
                <?php endif; ?>
            </div>
        <?php endif; ?>

        <!-- Hero Slider Images -->

        <div class="flexslider hero_slider_images hero_image_container">

            <ul class="slides">

                <?php foreach( $posts as $post): setup_postdata($post); ?>
                
                    <?php
                    $header_image_url = get_the_post_thumbnail_url($post->ID, 'large');
                    ?>
                    <li style="background-image: url(<?php echo $header_image_url ?>);">
                        <?php if(get_post_format() === 'video'): ?>
                            <a class="hero_image_link modal_trigger" data-modal="video_modal" data-video-type="<?php the_field('featured_video_type'); ?>" data-video-url="<?php the_field('featured_video_embed', false, false); ?>">
                                <i class="fas fa-play-circle"></i>
                            </a>
                        <?php else: ?>
                            <a href="<?php the_permalink(); ?>" class="hero_image_link"></a>
                        <?php endif; ?>
                    </li>

                <?php endforeach; wp_reset_postdata(); ?>

            </ul>

        </div>
        
        <!-- Hero Slider Content -->

        <div class="flexslider hero_slider_content hero_content_container">

            <div class="hero_content_box_container">

                <ul class="slides hero_content_box">

                    <?php foreach( $posts as $post): setup_postdata($post); ?>
                        <li class="hero_content">
                            <h3 class="category_name">
                                <?php tm_linked_category_name(get_the_category()[0]); ?>
                            </h3>
                            <div class="hero_content_title_container">
                                <?php if(get_post_format() === 'video'): ?>
                                    <h1><a class="modal_trigger" data-modal="video_modal" data-video-type="<?php the_field('featured_video_type'); ?>" data-video-url="<?php the_field('featured_video_embed', false, false); ?>"><?php echo tm_title_trim(get_the_title()); ?></a></h1>
                                <?php else: ?>
                                    <h1><a href="<?php the_permalink(); ?>"><?php echo tm_title_trim(get_the_title()); ?></a></h1>
                                <?php endif; ?>
                            </div>
                            <p><?php echo tm_excerpt_trim(get_the_excerpt()); ?></p>
                        </li>
                    <?php endforeach; wp_reset_postdata(); ?>

                </ul>

                <div class="hero_slider_navigation">

                    <div class="tm_flexslider_navigation <?php echo $nav_color_scheme; ?>">
                        <a href="#" class="flex-prev flex_arrow">
                            <i class="fal fa-long-arrow-left"></i>
                        </a>
                        <div class="tm_flexslider_controls_container"></div>
                        <a href="#" class="flex-next flex_arrow">
                            <i class="fal fa-long-arrow-right"></i>
                        </a>
                    </div>

                </div>

            </div>
        
        </div>

    <?php endif; ?>

</div>