<?php

/*-------------------------------------
Modal Video Container
---
Loaded in the header of each page. If a video modal is triggered in the DOM
then the contents of this modal will generate and play the video.
-------------------------------------*/

?>

<div class="modal video_modal">
    <div class="modal_transparent_layer modal_close"></div>
    <div class="modal_content">
        <div class="tm_plyr_video" data-plyr-provider="" data-plyr-embed-id=""></div>
    </div>
    <div class="modal_close">
        <i class="fal fa-times-circle"></i>
    </div>
</div>