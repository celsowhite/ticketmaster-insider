<?php 
// Get the page type so we know which sidebar to display.
$page_type = tm_get_page_type(get_queried_object()); ?>

<div class="tm_widget tm_ad_widget">
    <?php get_template_part('template-parts/component', 'rectangle_ad_unit'); ?>
</div>

<?php if($page_type === 'new-music'): ?>

    <?php dynamic_sidebar('new_music_sidebar'); ?>

<?php elseif($page_type === 'festival'): ?>
    
    <?php dynamic_sidebar('festivals_sidebar'); ?>

<?php elseif($page_type === 'minimaster'): ?>

    <?php dynamic_sidebar('minimaster_sidebar'); ?>

<?php else: ?>

    <?php dynamic_sidebar('default_sidebar'); ?>

<?php endif; ?>