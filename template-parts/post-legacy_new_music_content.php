<?php 
/*-----------------------------
Legacy New Music Post Content
---
Before the redesign TM added new music content in a specific way outside of the main content editor.
This code will render that content.
-----------------------------*/
?>

<?php if( get_field( 'general_info' )) { ?>
    <p class="general-section">
        <?php the_field('general_info'); ?>
    </p>
<?php } ?>
<?php if( get_field( 'video_embed' )) { ?>
    <p class="media-section">
        <span style="color: #009cde;"><big>Watch</big></span>
        <?php the_field('video_embed'); ?>
    </p>
<?php } ?>
<?php if( get_field( 'newsletter_image' )) { ?>
    <p class="newsletter-section">
        <a href="<?php the_field('newsletter_link'); ?>" target="_blank">
            <img src="<?php the_field('newsletter_image'); ?>" />
        </a>
    </p>
<?php } ?>
<?php if( get_field( 'tour_copy' )) { ?>
    <p class="tour-section">
        <span style="color: #009cde;"><big>Tour</big></span>
        <div><?php the_field('tour_copy'); ?></div>
        <?php if(get_field('tour_dates')): ?>
            <table class="events">
                <tbody>
                    
            
                    <?php while(has_sub_field('tour_dates')): ?>
                        <tr class="event" style="line-height: 40px;">
                            <td class="date" style="width: 80px;"><?php the_sub_field('date'); ?></td>
                            <td class="venue hidden-xs" style="margin: 0px 15px; display: block; "><?php the_sub_field('venue'); ?></td>
                            <td class="location"><?php the_sub_field('city_state'); ?></td>
                            </td>
                        </tr>
                    <?php endwhile; ?>
            
                    
                </tbody>
            </table>
        
        <?php endif; ?>
    </p>
<?php } ?>
<?php if( get_field( 'about' )) { ?>
    <p class="about-section">
        <span style="color: #009cde;"><big>About</big></span>
        <?php the_field('about'); ?>
    </p>
<?php } ?>
<?php if(get_field('facebook_widget')): ?>
    <h2>Facebook Page</h2>
    <p><?php the_field('facebook_widget'); ?></p>
<?php endif; ?>
<?php if(get_field('spotify_widget')): ?>
    <h2>Spotify Playlist</h2>
    <p><?php the_field('spotify_widget'); ?></p>
<?php endif; ?>
<?php if(get_field('twitter_widget')): ?>
    <h2>Twitter Page</h2>
    <p><?php the_field('twitter_widget'); ?></p>
<?php endif; ?>