<?php 
/*-----------------------------
Individual Post Header
---
Shows featured image, title and additional meta information.
-----------------------------*/
?>

<div class="hero_container blue">
    <div class="hero_image_container" style="background-image: url('<?php echo tm_get_post_thumbnail($post->ID, 'large'); ?>');"></div>
    <div class="hero_content_container">
        <div class="hero_content_box_container">
            <div class="hero_content_box">
                <div class="hero_content">
                    <?php get_template_part('template-parts/component', 'social_share_full'); ?>
                    <div class="hero_content_title_container">
                        <h1><?php echo tm_title_trim(get_the_title()); ?></h1>
                        <?php get_template_part('template-parts/post', 'author'); ?>
                    </div>
                    <div class="hero_meta">
                        <h3 class="category_name">
                            <?php tm_linked_category_name(get_the_category()[0]); ?>
                        </h3>
                        <p class="meta">
                            <?php echo get_the_date('M j, Y') ?>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>