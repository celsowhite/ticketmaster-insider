<?php 
/*-----------------------------
Upcoming Festivals Carousel
---
Shows a carousel of all of the upcoming popular festivals.
Currently featured on the homepage.
-----------------------------*/
?>

<h1 class="inpage_header">Upcoming Festivals</h1>

<div class="tm_carousel_container">

    <div class="tm_carousel">

        <?php
        $date_now = time();
        $date_yesterday = date('Y-m-d', strtotime('-1 day', $date_now));

        // Popular Festival ID's

        $festivals_page_id = tm_page_id_by_slug('festivals');

        $ids = get_field('popular_festivals', $festivals_page_id, false);

        $popular_festivals_loop_args = array(
            'post_type'         => 'tm_festival',
            'post__in'			=> $ids,
            'meta_query'        => array(
                array(
                    'key'       => 'event_start_date',
                    'value'     => $date_yesterday,
                    'compare'   => '>',
                    'type'      => 'DATE'
                )
            ),
            'meta_key'          => 'event_start_date',
            'orderby'           => 'meta_value',
            'order'             => 'ASC'
        );
        $popular_festivals_loop = new WP_Query($popular_festivals_loop_args);
        if ($popular_festivals_loop -> have_posts()) : while ($popular_festivals_loop -> have_posts()) : $popular_festivals_loop -> the_post();
        ?>

            <?php get_template_part('template-parts/card', 'festival_event'); ?>

        <?php endwhile; wp_reset_postdata(); endif; ?>

    </div>

    <div class="tm_carousel_nav"></div>

</div>

<a class="link_with_arrow" href="<?php echo page_link_by_slug('festivals'); ?>">
    <h3 class="spaced">View All</h3>
    <i class="fal fa-long-arrow-right"></i>
</a>