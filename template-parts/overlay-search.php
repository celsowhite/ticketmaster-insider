<div class="search_overlay">
    
    <div class="search_overlay_container">

        <?php get_search_form(); ?>

        <div class="trending_bar">
            <h3 class="spaced">Trending</h3>
            <?php 
            $trending_tags = get_field('trending_tags', 'option');
            if( $trending_tags ): ?>
                <ul class="tm_pills white">
                    <?php foreach( $trending_tags as $tag ): ?>
                        <li><a href="<?php echo get_term_link( $tag ); ?>">#<?php echo $tag->name; ?></a></li>
                    <?php endforeach; ?>
                </ul>
            <?php endif; ?>
        </div>
        
    </div>

</div>