 <?php
/*-------------------------------------
Minimaster Event Card
---
Shown in Minimaster event carousels.
-------------------------------------*/
?>


 <a class="event_card" href="<?php the_field('attraction_tickets_url'); ?>" target="_blank">
    <div class="event_card_image_container">
        <div class="event_card_image" style="background-image:url('<?php the_field('attraction_image'); ?>')"></div>
    </div>
    <div class="event_card_content">
        <p class="event_name"><?php the_title(); ?></p>
    </div>
</a>