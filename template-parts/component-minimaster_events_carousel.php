<?php 
/*-----------------------------
Minimaster Events Carousel
---
Shows a carousel of featured Minimaster events.
-----------------------------*/
?>

<?php 
$minimaster_page_id = tm_page_id_by_slug('minimaster-events');
$posts = get_field('featured_minimaster_events', $minimaster_page_id); 
if($posts): 
?>

    <!-- Minimaster Event Carousel -->

    <div class="tm_carousel_container">

        <div class="tm_carousel">

            <?php foreach( $posts as $post): setup_postdata($post); ?>
                    <?php get_template_part('template-parts/card', 'minimaster_event'); ?>
            <?php endforeach; wp_reset_postdata(); ?>

        </div>

        <div class="tm_carousel_nav"></div>

        <a class="link_with_arrow" href="<?php echo page_link_by_slug('minimaster-events'); ?>">
            <h3 class="spaced">View All</h3>
            <i class="fal fa-long-arrow-right"></i>
        </a>

    </div>

<?php endif; ?>