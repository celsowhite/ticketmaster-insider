<?php 
/* --------------------------
Related Posts
---
If this post has tags then show related posts by tag.
--------------------------*/
?>

<?php 
$tags = get_the_tags();
if($tags):
?>

    <div class="tm_section">
                    
        <div class="container">
            
            <h1 class="inpage_header">More Like This</h1>

            <div class="post_grid three_column">
                
                <?php
                // Save this posts tag ids.
                $tag_ids = array();
                foreach($tags as $individual_tag) {
                    $tag_ids[] = $individual_tag->term_id;
                }
                $related_posts_args = array (
                    'post_type'      => array('post'), 
                    'tag__in'        => $tag_ids,
                    'post__not_in'   => array($post->ID),
                    'posts_per_page' => 3
                );
                $related_posts_loop = new WP_Query($related_posts_args);
                if ($related_posts_loop -> have_posts()) :
                ?>
                    
                    <?php while ($related_posts_loop -> have_posts()) : $related_posts_loop -> the_post(); ?>

                        <?php get_template_part('template-parts/card', 'standard_post'); ?>
                    
                    <?php endwhile; wp_reset_postdata() ?>

                <?php endif; ?>

            </div>

            <!-- View More Tag Link -->

            <a class="link_with_arrow" href="<?php echo get_term_link($tags[0]); ?>">
                <h3 class="category_name">View More <?php echo $tags[0]->name; ?></h3>
                <i class="fal fa-long-arrow-right"></i>
            </a>

        </div>

    </div>

<?php endif; ?>