<?php

/*-------------------------------------
Lineup Modal
---
Shows the lineup from a specific festival
-------------------------------------*/

?>

<div class="modal lineup_modal">
    <div class="modal_transparent_layer modal_close"></div>
    <div class="modal_content">
        <h1 class="modal_title">Festival Name</h1>
        <div class="modal_body">Hey Celso</div>
    </div>
    <div class="modal_close">
        <i class="fal fa-times-circle"></i>
    </div>
</div>