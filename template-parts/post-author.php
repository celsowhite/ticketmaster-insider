<?php 
/*-----------------------------
Render Post Authors
---
Created a new way of assigning authors to posts via custom post type.
This template part will check if there are any authors added to the post.
If there aren't then it checks the legacy method of adding authors. In that legacy
method TM had an option to show/hide the byline so we also check for that bool 
*/
?>

<?php 
// New method
$authors = get_field('tm_authors');
if( $authors ): ?>

    <?php $author_names = array(); ?>

    <?php foreach( $authors as $author ): ?>

            <?php
                $url = get_the_permalink($author->ID);
                $title = get_the_title($author->ID); 
                $author_names[] = $title;
            ?>

    <?php endforeach; ?>

    <p class="meta"><?php echo implode(', ', $author_names); ?></p>

<?php 
// Old method
elseif(empty($authors) && get_field('display_byline')): ?>

	<p class="meta"><?php the_author() ?></p>

<?php endif; ?>