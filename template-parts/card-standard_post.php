<div class="post_card">

    <!-- Image -->

    <div class="post_card_image">
        <div class="post_card_image_block" style="background-image: url('<?php echo tm_get_post_thumbnail($post->ID, 'large'); ?>')"></div>
        <a href="<?php the_permalink(); ?>" class="post_card_image_link"></a>
        <div class="post_card_category bottom">
            <h3 class="category_name">
                <?php tm_linked_category_name(get_the_category()[0]); ?>
            </h3>
        </div>
    </div>

    <!-- Content -->

    <a href="<?php the_permalink(); ?>" class="post_card_content">
        <div class="post_card_content_top">
            <h2><?php echo tm_title_trim(get_the_title()); ?></h2>
            <p><?php echo tm_excerpt_trim(get_the_excerpt()); ?></p>
        </div>
        <?php get_template_part('template-parts/component', 'social_share_partial'); ?>
    </a>
    
</div>