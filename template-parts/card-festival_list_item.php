<?php

/*-------------------------------------
Festival List Item Card
---
Outputs a single festival card.
-------------------------------------*/

if(!isset($inventory_widget_type)) {
    $inventory_widget_type = '';
}

?>

<div class="inventory_widget_container <?php echo $inventory_widget_type; ?>">

    <div class="inventory_widget_header">
        <div class="inventory_widget_header_image" style="background-image: url('<?php the_field('event_image') ?>')"></div>
        <div class="inventory_widget_header_title">
            <p><a href="<?php the_field('event_tickets_url') ?>" target="_blank"><?php the_title(); ?></a></p>
        </div>
    </div>

    <ul class="inventory_widget_events">

        <li class="inventory_widget_event">

            <div class="event_info_container">

                <!-- Date/Time -->

                <div class="date_time_container">
                    <p class="date"><?php echo tm_event_date_format(get_field('event_start_date')); ?></p>
                </div>

                <div class="name_location_container">
                    <p class="location"><?php the_field('event_venue_city'); ?>, <?php the_field('event_venue_state'); ?> - <?php the_field('event_venue_name'); ?></p>
                    <?php if(get_field('event_attractions')): ?>
                        <p class="event_lineup"><a class="modal_trigger" data-modal="lineup_modal" data-title="<?php the_title(); ?>" data-lineup="<?php the_field('event_attractions'); ?>">Lineup</a></p>
                    <?php endif; ?>
                </div>

            </div>
            
            <!-- Tickets Button -->

            <div class="tickets_button_container">

                <?php 
                // Variables
                $tickets_url = get_field('event_tickets_url');
                $tickets_now_disclaimer_popup = 'While Ticketmaster does not sell tickets to this event, you can still get great resale tickets at our partner site TicketsNow. Please note resale prices often exceed face value.'; 
                $default_disclaimer_popup = "You are leaving the Ticketmaster website and linking to another ticket seller’s website.";
                ?>

                <?php 
                // Ticketmaster
                if(strpos($tickets_url,'ticketmaster.com')): ?>
                    <a class="tm_button small" href="<?php the_field('event_tickets_url') ?>" target="_blank">Find Tickets</a>
                <?php 
                // Ticketsnow
                elseif(strpos($tickets_url,'ticketsnow.com')): ?>
                    <a class="tm_button small" href="<?php the_field('event_tickets_url') ?>" target="_blank" data-balloon="<?php echo $tickets_now_disclaimer_popup; ?>" data-balloon-pos="left" data-balloon-length="large">Find Tickets</a>
                    <div class="button_subtext">
                        <p>On TicketsNow</p>
                        <p class="disclaimer">Resale prices often exceed face value.</p>
                    </div>
                <?php 
                // Livenation
                elseif(strpos($tickets_url,'livenation.com')): ?>
                    <a class="tm_button small" href="<?php the_field('event_tickets_url') ?>" target="_blank">Find Tickets</a>
                    <div class="button_subtext">
                        <p>On Live Nation</p>
                    </div>
                <?php 
                // Ticketweb
                elseif(strpos($tickets_url,'ticketweb.com')): ?>
                    <a class="tm_button small" href="<?php the_field('event_tickets_url') ?>" target="_blank" data-balloon="<?php echo $default_disclaimer_popup; ?>" data-balloon-pos="left" data-balloon-length="large">Find Tickets</a>
                    <div class="button_subtext">
                        <p>On TicketWeb</p>
                    </div>
                <?php 
                // Partner Site
                else: ?>
                    <a class="tm_button small" href="<?php the_field('event_tickets_url') ?>" target="_blank" data-balloon="<?php echo $default_disclaimer_popup; ?>" data-balloon-pos="left" data-balloon-length="large">Find Tickets</a>
                    <div class="button_subtext">
                        <p>On Partner Site</p>
                    </div>
                <?php endif; ?>

            </div>

        </li>

    </ul>

</div>