<div class="hero_container colorful">

    <?php $posts = get_field('featured_minimaster_events'); if($posts): ?>

        <!-- Sponsor -->

        <?php if(isset($sponsor)): ?>
            <div class="hero_sponsor">
                <h2>Presented By</h2>
                <div class="logo">
                    <?php 
                    $sponsor_logo_field = $sponsor . '_sponsor_logo'; 
                    $sponsor_link_field = $sponsor . '_sponsor_link';
                    $sponsor_link_object = get_field($sponsor_link_field, 'option');
                    if($sponsor_link_object):
                    $sponsor_link_url = $sponsor_link_object['url'];
                    $sponsor_link_target = $sponsor_link_object['target'];
                    ?>
                        <a href="<?php echo $sponsor_link_url; ?>" target="<?php echo $sponsor_link_target; ?>">
                            <img src="<?php echo get_field($sponsor_logo_field, 'option'); ?>" />
                        </a>
                    <?php else: ?>
                        <img src="<?php echo get_field($sponsor_logo_field, 'option'); ?>" />
                    <?php endif; ?>
                </div>
            </div>
        <?php endif; ?>
        
        <!-- Hero Slider Images -->

        <div class="flexslider hero_slider_images hero_image_container">

            <ul class="slides">

                <?php $image_slide_count = 0; foreach( $posts as $post): setup_postdata($post); ?>
                
                    <li style="background-image: url(<?php echo get_field('attraction_image') ?>);">
                        <?php if(get_post_format() === 'video'): ?>
                            <a class="hero_image_link modal_trigger" data-modal="video_modal" data-video-type="<?php the_field('featured_video_type'); ?>" data-video-url="<?php the_field('featured_video_embed', false, false); ?>">
                                <i class="fas fa-play-circle"></i>
                            </a>
                        <?php else: ?>
                            <a href="<?php echo get_field('attraction_tickets_url'); ?>" class="hero_image_link" target="_blank"></a>
                        <?php endif; ?>
                    </li>

                    <?php
                    $image_slide_count++;
                    if($image_slide_count == 4) break;
                    ?>

                <?php endforeach; wp_reset_postdata(); ?>

            </ul>

        </div>
        
        <!-- Hero Slider Content -->

        <div class="flexslider hero_slider_content hero_content_container">

            <div class="hero_content_box_container">

                <ul class="slides hero_content_box">

                    <?php $content_slide_count = 0; foreach( $posts as $post): setup_postdata($post); ?>
                    
                        <li class="hero_content">
                            <h3 class="category_name">
                                <?php echo get_the_terms($post->ID, 'tm_minimaster_event_type')[0]->name; ?>
                            </h3>
                            <div class="hero_content_title_container">
                                <h1><a href="<?php the_field('attraction_tickets_url'); ?>" target="_blank"><?php echo tm_title_trim(get_the_title()); ?></a></h1>
                            </div>
                            <p><?php echo tm_excerpt_trim(get_the_excerpt()); ?></p>
                        </li>

                        <?php
                        $content_slide_count++;
                        if($content_slide_count == 4) break;
                        ?>

                    <?php endforeach; wp_reset_postdata(); ?>

                </ul>

                <div class="hero_slider_navigation">

                    <div class="tm_flexslider_navigation white">
                        <a href="#" class="flex-prev flex_arrow">
                            <i class="fal fa-long-arrow-left"></i>
                        </a>
                        <div class="tm_flexslider_controls_container"></div>
                        <a href="#" class="flex-next flex_arrow">
                            <i class="fal fa-long-arrow-right"></i>
                        </a>
                    </div>

                </div>

            </div>
        
        </div>

    <?php endif; ?>

</div>