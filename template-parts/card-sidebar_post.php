<?php
// We sometimes use this template part outside of the loop.
// If that is the case then we pass the post id.
// If the post_id variable isn't set then we can assume the template part is being used in the loop
// and we can use the standard $post variable.

if(!isset($post_id)) {
    $post_id = $post->ID;
}
?>

<div class="sidebar_post_card">

    <!-- Image -->

    <div class="sidebar_post_card_image">
        <div class="sidebar_post_card_image_block" style="background-image: url('<?php echo tm_get_post_thumbnail($post_id, 'large'); ?>')"></div>
        <a href="<?php the_permalink($post_id); ?>" class="post_card_image_link"></a>
        <div class="post_card_category top pink">
            <h3 class="category_name"><?php echo get_the_category($post_id)[0]->cat_name; ?></h3>
        </div>
    </div>

    <!-- Content -->

    <a href="<?php the_permalink($post_id); ?>" class="sidebar_post_card_content">
        <h2><?php echo tm_title_trim(get_the_title($post_id)); ?></h2>
    </a>
    
</div>