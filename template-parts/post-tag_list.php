<?php 
/*-----------------------------
Tag List
---
Shows the post tags in the TM pills format.
-----------------------------*/
?>

<?php
$post_tags = get_the_tags();
if($post_tags):
?>

    <h3>Tags</h3>

    <ul class="tm_pills flush_left">
        <?php foreach($post_tags as $tag): ?>
            <li><a href="<?php echo get_term_link($tag); ?>">#<?php echo $tag->name; ?></a></li>
        <?php endforeach; ?>
    </ul>

<?php endif; ?>