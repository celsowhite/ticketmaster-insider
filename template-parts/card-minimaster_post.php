<?php if(get_post_format() === 'video'): ?>

    <div class="post_card minimaster">

        <!-- Image -->

        <div class="post_card_image">
            <div class="post_card_image_block" style="background-image: url('<?php echo tm_get_post_thumbnail($post->ID, 'large'); ?>')"></div>
            <a class="post_card_image_link modal_trigger" data-modal="video_modal" data-video-type="<?php the_field('featured_video_type'); ?>" data-video-url="<?php the_field('featured_video_embed', false, false); ?>"></a>
            <div class="post_card_category with_icon bottom">
                <i class="fas fa-play-circle"></i>
                <h3 class="category_name"><?php echo get_the_category()[0]->cat_name; ?></h3>
            </div>
        </div>

        <!-- Content -->

        <div class="post_card_content modal_trigger" data-modal="video_modal" data-video-type="<?php the_field('featured_video_type'); ?>" data-video-url="<?php the_field('featured_video_embed', false, false); ?>">
            <h2><a class="modal_trigger" data-modal="video_modal" data-video-type="<?php the_field('featured_video_type'); ?>" data-video-url="<?php the_field('featured_video_embed', false, false); ?>"><?php echo tm_title_trim(get_the_title()); ?></a></h2>
            <p><?php echo tm_excerpt_trim(get_the_excerpt()); ?></p>
        </div>
        
    </div>

<?php else: ?>

    <div class="post_card minimaster">

        <!-- Image -->

        <div class="post_card_image">
            <div class="post_card_image_block" style="background-image: url('<?php echo tm_get_post_thumbnail($post->ID, 'large'); ?>')"></div>
            <a href="<?php the_permalink(); ?>" class="post_card_image_link"></a>
            <div class="post_card_category with_icon bottom">
                <i class="fas fa-clone"></i>
                <h3 class="category_name"><?php echo get_the_category()[0]->cat_name; ?></h3>
            </div>
        </div>

        <!-- Content -->

        <a href="<?php the_permalink(); ?>" class="post_card_content">
            <div class="post_card_content_top">
                <h2><?php echo tm_title_trim(get_the_title()); ?></h2>
                <p><?php echo tm_excerpt_trim(get_the_excerpt()); ?></p>
            </div>
            <?php get_template_part('template-parts/component', 'social_share_partial'); ?>
        </a>
        
    </div>

<?php endif; ?>