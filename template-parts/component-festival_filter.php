<div class="festival_filter_form">

    <!-- Location Dropdown -->

    <div class="tm_dropdown festival_location_dropdown">
        <div class="tm_dropdown_button">
            <span class="tm_dropdown_selection">Location</span>
            <i class="fas fa-caret-down"></i>
        </div>
        <ul class="tm_dropdown_list">
            <li data-type="country" data-value="United States Of America">All United States</li>
            <li data-type="country" data-value="Canada">All Canada</li>
            <li data-type="state" data-value="AL">Alabama</li>
            <li data-type="state" data-value="AK">Alaska</li>
            <li data-type="state" data-value="AB">Alberta</li>
            <li data-type="state" data-value="AZ">Arizona</li>
            <li data-type="state" data-value="AR">Arkansas</li>
            <li data-type="state" data-value="BC">British Columbia</li>
            <li data-type="state" data-value="CA">California</li>
            <li data-type="state" data-value="CO">Colorado</li>
            <li data-type="state" data-value="CT">Connecticut</li>
            <li data-type="state" data-value="DE">Delaware</li>
            <li data-type="state" data-value="DC">District Of Columbia</li>
            <li data-type="state" data-value="FL">Florida</li>
            <li data-type="state" data-value="GA">Georgia</li>
            <li data-type="state" data-value="HI">Hawaii</li>
            <li data-type="state" data-value="ID">Idaho</li>
            <li data-type="state" data-value="IL">Illinois</li>
            <li data-type="state" data-value="IN">Indiana</li>
            <li data-type="state" data-value="IA">Iowa</li>
            <li data-type="state" data-value="KS">Kansas</li>
            <li data-type="state" data-value="KY">Kentucky</li>
            <li data-type="state" data-value="LA">Louisiana</li>
            <li data-type="state" data-value="ME">Maine</li>
            <li data-type="state" data-value="MB">Manitoba</li>
            <li data-type="state" data-value="MD">Maryland</li>
            <li data-type="state" data-value="MA">Massachusetts</li>
            <li data-type="state" data-value="MI">Michigan</li>
            <li data-type="state" data-value="MN">Minnesota</li>
            <li data-type="state" data-value="MS">Mississippi</li>
            <li data-type="state" data-value="MO">Missouri</li>
            <li data-type="state" data-value="MT">Montana</li>
            <li data-type="state" data-value="NE">Nebraska</li>
            <li data-type="state" data-value="NV">Nevada</li>
            <li data-type="state" data-value="NB">New Brunswick</li>
            <li data-type="state" data-value="NH">New Hampshire</li>
            <li data-type="state" data-value="NJ">New Jersey</li>
            <li data-type="state" data-value="NM">New Mexico</li>
            <li data-type="state" data-value="NY">New York</li>
            <li data-type="state" data-value="NL">Newfoundland</li>
            <li data-type="state" data-value="NC">North Carolina</li>
            <li data-type="state" data-value="ND">North Dakota</li>
            <li data-type="state" data-value="NT">Northwest Territories</li>
            <li data-type="state" data-value="NS">Nova Scotia</li>
            <li data-type="state" data-value="NU">Nunavut</li>
            <li data-type="state" data-value="OH">Ohio</li>
            <li data-type="state" data-value="OK">Oklahoma</li>
            <li data-type="state" data-value="ON">Ontario</li>
            <li data-type="state" data-value="PA">Pennsylvania</li>
            <li data-type="state" data-value="PE">Prince Edward Island</li>
            <li data-type="state" data-value="QC">Quebec</li>
            <li data-type="state" data-value="RI">Rhode Island</li>
            <li data-type="state" data-value="SK">Saskatchewan</li>
            <li data-type="state" data-value="SC">South Carolina</li>
            <li data-type="state" data-value="SD">South Dakota</li>
            <li data-type="state" data-value="TN">Tennessee</li>
            <li data-type="state" data-value="TX">Texas</li>
            <li data-type="state" data-value="UT">Utah</li>
            <li data-type="state" data-value="VT">Vermont</li>
            <li data-type="state" data-value="VA">Virginia</li>
            <li data-type="state" data-value="WA">Washington</li>
            <li data-type="state" data-value="WV">West Virginia</li>
            <li data-type="state" data-value="WI">Wisconsin</li>
            <li data-type="state" data-value="WY">Wyoming</li>
            <li data-type="state" data-value="YT">Yukon</li>
        </ul>
    </div>

    <!-- Month Dropdown -->

    <div class="tm_dropdown festival_month_dropdown">
        <div class="tm_dropdown_button">
            <span class="tm_dropdown_selection">Month</span>
            <i class="fas fa-caret-down"></i>
        </div>
        <ul class="tm_dropdown_list">
            <li data-value="January">January</li>
            <li data-value="February">February</li>
            <li data-value="March">March</li>
            <li data-value="April">April</li>
            <li data-value="May">May</li>
            <li data-value="June">June</li>
            <li data-value="July">July</li>
            <li data-value="August">August</li>
            <li data-value="September">September</li>
            <li data-value="October">October</li>
            <li data-value="November">November</li>
            <li data-value="December">December</li>
        </ul>
    </div>

    <!-- Filter Button -->

    <button class="tm_button_rounded">Filter</button>

</div>