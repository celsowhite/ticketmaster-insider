<!-- Facebook SDK -->

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.12&appId=526024987506828&autoLogAppEvents=1';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<!-- #BlogTagManager -->
<!-- Google Tag Manager -->

<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-W3RWP4" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>

<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        '//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-W3RWP4');</script>
	
<!-- #TicketmasterTagManager -->
<!-- Google Tag Manager -->

<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-K4QMLG"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-K4QMLG');</script>

<!-- Begin Monetate ExpressTag Sync v8. Place at start of document head. DO NOT ALTER. -->

<script type="text/javascript">
	/* var monetateT = new Date().getTime();
	(function() {
		var p = document.location.protocol;
		if (p == "http:" || p == "https:") {
			var m = document.createElement("script"); m.type = "text/javascript"; m.src = (p == "https:" ? "https://s" : "http://") + "e.monetate.net/js/2/a-a1627c0e/p/ticketmaster.com/entry.js";
			var e = document.createElement("div"); e.appendChild(m); document.write(e.innerHTML);
		}
	})(); */
</script>

<!-- Google Tag Manager Ads -->

<script type="text/javascript">
	var googletag = googletag || {};
	googletag.cmd = googletag.cmd || [];
	(function() {
	var gads = document.createElement("script");
	gads.async = true;
	gads.type = "text/javascript";
	var useSSL = "https:" == document.location.protocol;
	gads.src = (useSSL ? "https:" : "http:") + "//www.googletagservices.com/tag/js/gpt.js";
	var node =document.getElementsByTagName("script")[0];
	node.parentNode.insertBefore(gads, node);
	})();
</script>

<script type='text/javascript'>

	googletag.cmd.push(function() {
	
		googletag.defineSlot('/6025/us/blog', [300, 250], 'insider-ad-slot-502')
			.addService(googletag.pubads())
			.setTargeting("page","blog")
			.setTargeting("pagepos", "502");

		googletag.defineSlot('/6025/us/festival', [300, 250], 'festival-ad-slot-502')
			.addService(googletag.pubads())
			.setTargeting("page","home")
			.setTargeting("pagepos", "502");

		googletag.defineSlot('/6025/us/insider/minimaster', [[300, 250], [320, 50]], 'minimaster-ad-slot-502')
			.addService(googletag.pubads())
			.defineSizeMapping(googletag.sizeMapping()
			.addSize([1024, 768],[300, 250]).addSize([800, 600],[300, 250]).addSize([0, 0],[320, 50]).build())
			.setTargeting("page","minimaster")
			.setTargeting("pagepos", "502");

		googletag.enableServices();

	});

</script>