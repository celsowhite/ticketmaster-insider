<?php
/**
 * Featured post header on tag and category pages.
 */
?>

<div class="hero_container">
    <div class="hero_image_container" style="background-image: url('<?php echo tm_get_post_thumbnail($post->ID, 'large'); ?>');">
        <a href="<?php the_permalink(); ?>" class="hero_image_link"></a>
    </div>
    <div class="hero_content_container">
        <div class="hero_content_box_container">
            <div class="hero_content_box">
                <div class="hero_content">
                    <h3 class="category_name">
                        <?php tm_linked_category_name(get_the_category()[0]); ?>
                    </h3>
                    <div class="hero_content_title_container">
                        <h1><a href="<?php the_permalink(); ?>"><?php echo tm_title_trim(get_the_title()); ?></a></h1>
                    </div>
                    <p><?php echo tm_excerpt_trim(get_the_excerpt()); ?></p>
                </div>
            </div>
        </div>
    </div>
</div>