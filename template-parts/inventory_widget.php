<?php 

/*-------------------------------------
TM Inventory Widget
---
TM team can use the backend ACF module to enter different parameters from the Discovery API.
Based on their inputs this widget will render with matching events.
API query is done dynamically with js.
-------------------------------------*/

// Save inventory inputs.

$inventory_widget_title = get_field('inventory_widget_title', $inventory_widget_id);
$attraction_id = get_field('inventory_widget_attraction_id', $inventory_widget_id);
$country_code = get_field('inventory_widget_country_code', $inventory_widget_id);
$venue_id = get_field('inventory_widget_venue_id', $inventory_widget_id);
$classification_id = get_field('inventory_widget_classification_id', $inventory_widget_id);
$keyword = get_field('inventory_widget_keyword', $inventory_widget_id);
$locationType = get_field('inventory_widget_location_type', $inventory_widget_id);
$market_id = get_field('inventory_widget_market_id', $inventory_widget_id);
$dma_id = get_field('inventory_widget_dma_id', $inventory_widget_id);
$start_date_time = get_field('inventory_widget_start_date_time', $inventory_widget_id);
$start_date_time_formatted = date("Y-m-d\TH:i:s\Z", strtotime($start_date_time));
$end_date_time = get_field('inventory_widget_end_date_time', $inventory_widget_id);	
$end_date_time_formatted = date("Y-m-d\TH:i:s\Z", strtotime($end_date_time));
$size = get_field('inventory_widget_size', $inventory_widget_id);
            
?>

<?php if($inventory_widget_title): ?>
    <div 
        class="inventory_widget_container dynamically_populated" 
        data-attraction-id="<?php echo $attraction_id; ?>" 
        data-venue-id="<?php echo $venue_id; ?>"
        data-classification-id="<?php echo $classification_id; ?>"
        data-keyword="<?php echo $keyword; ?>"
        <?php if($locationType === 'market'): ?>
            data-market-id="<?php echo $market_id; ?>"
        <?php endif; ?>
        <?php if($locationType === 'country'): ?>
            data-country-code="<?php echo $country_code; ?>"
        <?php endif; ?>
        <?php if($locationType === 'dma'): ?>
            data-dma-id="<?php echo $dma_id; ?>"
        <?php endif; ?>
        <?php if($locationType === 'geolocation'): ?>
            data-geolocation="true"
        <?php endif; ?>
        <?php if($start_date_time): ?>
            data-start-date-time="<?php echo $start_date_time_formatted; ?>"
        <?php endif; ?>
        <?php if($end_date_time): ?>
            data-end-date-time="<?php echo $end_date_time_formatted; ?>"
        <?php endif; ?>
        data-size="<?php echo $size; ?>"
    >

        <div class="inventory_widget_header">
            <div class="inventory_widget_header_image"></div>
            <div class="inventory_widget_header_title">
                <p><?php echo $inventory_widget_title; ?></p>
                <div class="inventory_widget_sub_title"></div>
            </div>
            <i class="fal fa-spinner inventory_widget_loading"></i>
        </div>

        <ul class="inventory_widget_events"></ul>

    </div>

<?php endif; ?>