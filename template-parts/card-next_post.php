<?php
/*---------------------
Next Post
---
Next post card shown on single post pages.
Dynamically shown via js at the end of a post.
---------------------*/
?>

<?php
$next_post = get_adjacent_post(true, '', true, 'post_tag');
?>

<div class="next_post_card_container hidden">

    <div class="next_post_card">

        <!-- Image -->

        <a href="<?php the_permalink($next_post->ID); ?>" class="next_post_card_image" style="background-image: url('<?php echo tm_get_post_thumbnail($next_post->ID, 'medium'); ?>');"></a>

        <!-- Content -->

        <a class="next_post_card_content" href="<?php the_permalink($next_post->ID); ?>">
            <div class="link_with_arrow">
                <h3 class="spaced">Next Article</h3>
                <i class="fal fa-long-arrow-right"></i>
            </div>
            <h2><?php echo tm_title_trim(get_the_title($next_post->ID)); ?></h2>
        </a>
        
    </div>

</div>