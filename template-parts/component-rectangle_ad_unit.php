<?php 
// Get the page type so we know which sidebar to display.
$page_type = tm_get_page_type(get_queried_object()); 
?>

<?php 
// If ad type should be new music and their is a custom ad set then display it.
if($page_type === 'new-music' && get_field('new_music_rectangle_ad_image', 'option')): ?>

    <?php 
    $ad_image = get_field('new_music_rectangle_ad_image', 'option');
    $ad_link = get_field('new_music_rectangle_ad_link', 'option'); 
    ?>  

    <a href="<?php echo $ad_link['url'] ?>" target="<?php echo $ad_link['target']; ?>">
        <img src="<?php echo $ad_image; ?>" alt="<?php echo $ad_link['title']; ?>" />
    </a>

<?php
elseif($page_type === 'festival'): ?>

    <?php 
    if(get_field('festival_rectangle_ad_image', 'option')): 
    $ad_image = get_field('festival_rectangle_ad_image', 'option');
    $ad_link = get_field('festival_rectangle_ad_link', 'option');
    ?>

        <a href="<?php echo $ad_link['url'] ?>" target="<?php echo $ad_link['target']; ?>">
            <img src="<?php echo $ad_image; ?>" alt="<?php echo $ad_link['title']; ?>" />
        </a>

    <?php else: ?>
        <div id="festival-ad-slot-502">
            <script type="text/javascript">
                googletag.cmd.push(function() { googletag.display('festival-ad-slot-502'); });
            </script>
        </div>
    <?php endif; ?>

<?php
elseif($page_type === 'minimaster'): ?>

    <?php 
    if(get_field('minimaster_rectangle_ad_image', 'option')): 
    $ad_image = get_field('minimaster_rectangle_ad_image', 'option');
    $ad_link = get_field('minimaster_rectangle_ad_link', 'option');
    ?>

        <a href="<?php echo $ad_link['url'] ?>" target="<?php echo $ad_link['target']; ?>">
            <img src="<?php echo $ad_image; ?>" alt="<?php echo $ad_link['title']; ?>" />
        </a>

    <?php else: ?>
        <div id="minimaster-ad-slot-502">
            <script type="text/javascript">
                googletag.cmd.push(function() { googletag.display('minimaster-ad-slot-502'); });
            </script>
        </div>
    <?php endif; ?>

<?php 
// Else show the standard rectangle ad.
else: ?>
    
    <div id="insider-ad-slot-502">
        <script type="text/javascript">
            googletag.cmd.push(function() { googletag.display('insider-ad-slot-502'); });
        </script>
    </div>

<?php endif; ?>